// https://jamiemason.github.io/syncpack/config/semver-range
module.exports = {
  dependencyTypes: ['dev', 'prod'],
  semverRange: '^',
  source: ['package.json', 'packages-*/*/package.json'],
  versionGroups: [
    {
      label: 'Internal config packages should be pinned to "*" (meaning any version is acceptable)',
      packages: ['**'],
      dependencies: ['lib-prettier', 'lib-eslint-config', 'lib-spellcheck', 'lib-logger'],
      dependencyTypes: ['workspace'],
      pinVersion: '*',
    },
  ],
  // Sort package.json file structure based off opinionated formatter: https://www.npmjs.com/package/prettier-package-json
  // https://github.com/cameronhunter/prettier-package-json/blob/HEAD/src/defaultOptions.ts
  sortFirst: [
    // Details
    '$schema',
    'name',
    'version',
    'description',
    'private',
    'license',
    'author',
    'maintainers',
    'contributors',
    'homepage',
    'repository',
    'bugs',
    'type',
    // Configuration
    'exports',
    'main',
    'module',
    'browser',
    'man',
    'preferGlobal',
    'bin',
    'files',
    'directories',
    'scripts',
    'config',
    'sideEffects',
    'types',
    'typings',
    // Yarn specific
    'workspaces',
    'resolutions',
    // Dependencies
    'dependencies',
    'bundleDependencies',
    'bundledDependencies',
    'peerDependencies',
    'peerDependenciesMeta',
    'optionalDependencies',
    'devDependencies',
    // Used for npm search
    'keywords',
    // Constraints
    'engines',
    'engineStrict',
    'os',
    'cpu',
    'packageManager',
    // Package publishing configuration
    'publishConfig',
  ],
};
