# JSON Resume

## Documentation

[API] <-> [Frontend]
[Openapi]

Languages:

- Javascript

Framework:

- NextJS / NodeJS 14
- Docker
- Cloud Functions
- Cloud functions framework (for local development)
- Lerna - For packaging APIs

Pricing:

- As free as possible

## Testing

```shell
./node_modules/mocha/bin/mocha.js --config packages-test/test-integration/.mocharc.js **/*.spec.js --ignore 'node_modules/**/*'
```

## Links

- <https://buttercms.com/blog/nextjs-storybook-and-lerna-build-a-monorepo-structure>
- <https://daveteu.medium.com/nextjs-with-google-cloud-functions-in-10-minutes-47f88e6ac27a>
- <https://www.velotio.com/engineering-blog/scalable-front-end-with-lerna-yarn-react>
- <https://verdaccio.org/blog/2019/09/07/managing-multiples-projects-with-lerna-and-yarn-workspaces/#managing-dependencies-and-devdependencies>
- <https://github.com/anthonyhastings/turborepo-design-system/tree/main>
- <https://github.com/joe-bell/plaiceholder/blob/main/package.json>
- <https://www.conventionalcommits.org/en/v1.0.0/>
- <https://classic.yarnpkg.com/en/docs/workspaces>
- <https://medium.com/@jsilvax/automate-semantic-versioning-with-conventional-commits-d76a9f45f2fa>
- <https://dev.to/davipon/add-commitint-commitizen-standard-version-and-husky-to-sveltekit-project-14pc>
- <https://www.honeybadger.io/blog/monorepo-yarn-workspace-lerna/>

### UI Design

- https://github.com/mui/material-ui/tree/master/examples/material-ui-nextjs
- https://mui.com/material-ui/guides/next-js-app-router/
