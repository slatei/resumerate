/*
 * lint-staged appends the staged file paths to the end of the executed command
 * Capture and map the filename array to a suitable string for the prettier and syncpack
 * https://github.com/okonet/lint-staged#configuration
 * https://github.com/joe-bell/plaiceholder/blob/main/.config/lint-staged.config.mjs
 */
module.exports = {
  '**/*': 'cspell --no-must-find-files',
  '**/*.{bash,sh}': 'npx shellcheck',
  // '**/*.js': 'jest --bail --findRelatedTests',
  '**/*.{js,json,md,yaml}': 'prettier --write',
  '**/*.{md,.markdown}': 'markdownlint-cli2 --fix',
  '**/package.json': (filenames) => [
    `prettier --write ${filenames.join(' ')}`,
    `syncpack format ${filenames.map((filename) => `--source '${filename}'`).join(' ')}`,
  ],
};
