const path = require('path');
const OpenApiValidator = require('express-openapi-validator');

const resumeSpecPath = path.join(__dirname, 'specifications', 'resume.openapi.yml');
const testSpecPath = path.join(__dirname, 'specifications', 'test.openapi.yml');

module.exports = {
  ResumeValidator: OpenApiValidator.middleware({
    apiSpec: resumeSpecPath,
    validateRequests: true,
    validateResponses: true,
  }),
  TestValidator: OpenApiValidator.middleware({
    apiSpec: testSpecPath,
    validateRequests: true,
    validateResponses: true,
  }),
};
