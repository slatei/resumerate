const api = require('./index.api');
const db = require('./index.db');
const auth = require('./index.auth');
const email = require('./index.email');

module.exports = {
  api,
  db,
  auth,
  email,
};
