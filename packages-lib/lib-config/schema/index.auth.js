module.exports = {
  JWT_SECRET: {
    doc: 'JWT secret key used for signing tokens',
    format: String,
    default: '',
    env: 'JWT_SECRET',
  },
  JWT_ACCESS_EXPIRATION_MINUTES: {
    doc: 'Minutes after which access tokens expire',
    format: Number,
    default: 30,
  },
  JWT_REFRESH_EXPIRATION_DAYS: {
    doc: 'Days after which refresh tokens expire',
    format: Number,
    default: 30,
  },
  JWT_RESET_PASSWORD_EXPIRATION_MINUTES: {
    doc: 'Minutes after which the refresh password token expires',
    format: Number,
    default: 10,
  },
  JWT_VERIFY_EMAIL_EXPIRATION_MINUTES: {
    doc: 'Minutes after which verify email token expires',
    format: Number,
    default: 10,
  },
};
