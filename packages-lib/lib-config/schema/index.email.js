module.exports = {
  SERVICE_ENABLED: {
    doc: 'Whether or not to attach to the email services',
    format: Boolean,
    default: false,
  },
  SMTP_HOST: {
    doc: 'Server that will send the emails',
    format: String,
    default: 'smtp.ethereal.email',
  },
  SMTP_PORT: {
    doc: 'Port to connect to the email server',
    format: Number,
    default: 587,
  },
  SMTP_USERNAME: {
    doc: 'Username for email server',
    format: String,
    default: '',
    env: 'SMTP_USERNAME',
  },
  SMTP_PASSWORD: {
    doc: 'Password for email server',
    format: String,
    default: '',
    env: 'SMTP_PASSWORD',
  },
  FROM_ADDRESS: {
    doc: 'The "From" field in the emails sent by the app',
    format: String,
    default: 'support@thorpincorp.com',
  },
};
