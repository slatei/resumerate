const DEFAULT_PORT = 8088;

module.exports = {
  PORT: {
    doc: 'The port for the API to listen on.',
    format: 'port',
    default: DEFAULT_PORT,
    env: 'PORT',
    arg: 'port',
  },
  HOST_URL: {
    doc: 'Api server host address',
    format: String,
    default: `https://localhost:${DEFAULT_PORT}`,
  },
  SSL_ENABLED: {
    doc: 'Configure the API to use SSL',
    format: Boolean,
    default: true,
  },

  LOG_LEVEL: {
    doc: 'The logging level',
    default: 'info',
    format: String,
  },
  LOG_PRETTY_PRINT: {
    doc: 'Whether to print logs as human readable',
    default: false,
    format: Boolean,
    env: 'LOG_PRETTY_PRINT',
  },
};
