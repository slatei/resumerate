module.exports = {
  MONGODB_HOST: {
    doc: 'Database host name/IP',
    format: String,
    default: 'mongodb://localhost:27017',
    env: 'MONGODB_HOST',
  },
  MONGODB_COLLECTION_NAME: {
    doc: 'Database collection name',
    format: String,
    default: 'resume',
  },
  MONGODB_USERNAME: {
    doc: 'Database access username',
    format: String,
    default: '',
    env: 'MONGODB_USERNAME',
  },
  MONGODB_PASSWORD: {
    doc: 'Database access password',
    format: String,
    default: '',
    env: 'MONGODB_PASSWORD',
  },
};
