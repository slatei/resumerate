const development = require('./env.development');
const production = require('./env.production');

module.exports = {
  production,
  development,
};
