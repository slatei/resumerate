const path = require('path');
const convict = require('convict');
const envs = require('./envs');
const globalSchema = require('./schema');

require('dotenv').config({ path: path.join(__dirname, '.env') });

convict.addFormat(require('convict-format-with-validator').ipaddress);

exports.createConfig = createConfig;
exports.getConfigEnvironment = getConfigEnvironment;
exports.config = createConfig();

function getConfigEnvironment() {
  if (process.env.NODE_ENV === 'production') {
    return 'production';
  }
  return 'development';
}

function createConfig(schema) {
  const defaultConfig = {
    env: {
      doc: 'The application environment.',
      format: ['production', 'development', 'test'],
      default: 'development',
      env: 'NODE_ENV',
    },
  };
  const config = convict({ ...defaultConfig, ...schema, ...globalSchema });
  config.load(envs[getConfigEnvironment()]);

  config.validate({ allowed: 'strict' });

  return config;
}
