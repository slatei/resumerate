const https = require('https');
const http = require('http');
const fs = require('fs');
const { errorHandler } = require('@resumapi/lib-error');

async function startHttpServer({ app, name, port, logger, sslCertPath, sslKeyPath, sslCert, sslKey }) {
  logger.info(`Starting '${name}' on port ${port}`);

  const sslOptions = constructSslOptions({
    certPath: sslCertPath,
    keyPath: sslKeyPath,
    cert: sslCert,
    key: sslKey,
    logger,
  });

  // Start http/s server
  createServer({ app, port, name, logger, sslOptions })
    .then(() => handleSuccess({ app, port, logger }))
    .catch((err) => errorHandler.handleError(err, { app, logger }));
}
exports.startHttpServer = startHttpServer;

async function createServer({ app, name, port, sslOptions, logger }) {
  const server = await startAppListening({ app, name, port, sslOptions, logger });

  // https://cloud.google.com/load-balancing/docs/https#timeouts_and_retries
  server.keepAliveTimeout = 600 * 1000;
}

async function startAppListening({ app, port, name, sslOptions, logger }) {
  return new Promise((resolve, reject) => {
    // https://nodejs.org/api/https.html#https_https_createserver_options_requestlistener
    const server = sslOptions ? https.createServer(sslOptions, app) : http.createServer(app);

    errorHandler.listenToErrorEvents(server);

    server.listen(port, (err) => {
      if (err) {
        return reject(err);
      }

      logger.info(`App '${name}' listening on port ${port} with SSL ${sslOptions ? 'enabled' : 'disabled'}`);

      // Set Timeout here to ensure that the server variable is assigned
      return setTimeout(() => resolve(server));
    });
  });
}

function handleSuccess({ port, logger }) {
  logger.info('Successfully created server', { port });
}

function constructSslOptions({ certPath, keyPath, cert, key, logger }) {
  if ((!certPath || !keyPath) && (!cert || !key)) {
    return undefined;
  }

  try {
    return {
      cert: cert || fs.readFileSync(certPath),
      key: key || fs.readFileSync(keyPath),
    };
  } catch (err) {
    logger.error(`Error retrieving server certificates`, err);
    throw new Error('Failed to retrieve server certificates');
  }
}
