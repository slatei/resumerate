const util = require('util');
const logger = require('@resumapi/lib-logger');
const {
  APIValidationError,
  ProblemDetailsError,
  DatabaseError,
  NotFoundError,
  ForbiddenError,
  ConflictError,
  UnauthorizedError,
  BadRequestError,
  RequestValidationError,
} = require('./common');

let httpServerRef;

const errorHandler = {
  // Listen to the global process-level error events
  // https://github.com/goldbergyoni/nodebestpractices/blob/master/sections/errorhandling/catchunhandledpromiserejection.md
  listenToErrorEvents: (httpServer) => {
    httpServerRef = httpServer;
    process.on('uncaughtException', async (error) => {
      await errorHandler.handleError(error);
    });

    process.on('unhandledRejection', async (reason) => {
      await errorHandler.handleError(reason);
    });

    process.on('SIGTERM', async () => {
      logger.error('App received SIGTERM event, try to gracefully close the server');
      await terminateHttpServerAndExit();
    });

    process.on('SIGINT', async () => {
      logger.error('App received SIGINT event, try to gracefully close the server');
      await terminateHttpServerAndExit();
    });
  },

  // Before modification, this would only handle non-operational error events
  // A "final" error handler middleware is also required to return the error response
  handleError: (error) => {
    try {
      logger.warn(error);
      const appError = normalizeError(error);

      // A common best practice is to crash when an unknown error (non-trusted) is being thrown
      if (!appError?.isOperational) {
        terminateHttpServerAndExit(appError);
      }
    } catch (handlingError) {
      // Not using the logger here because it might have failed
      process.stdout.write(
        'The error handler failed, here is the handler failure and then the origin error that it tried to handle',
      );
      process.stdout.write(JSON.stringify(handlingError));
      process.stdout.write(JSON.stringify(error));
    }
  },

  // eslint-disable-next-line no-unused-vars
  handleErrorMiddleware: (err, req, res, next) => {
    try {
      logger.warn(err);
      const appError = normalizeError(err);

      // TODO - use handleError here

      res.setHeader('Content-type', 'application/json; charset=utf-8');

      res.writeHead(appError.status);

      const errObj = reformatError(appError);

      res.end(errObj !== undefined ? JSON.stringify(errObj) : undefined);
    } catch (handlingError) {
      // Not using the logger here because it might have failed
      process.stdout.write(
        'The error handler failed, here is the handler failure and then the origin error that it tried to handle',
      );
      process.stdout.write(JSON.stringify(handlingError));
      process.stdout.write(JSON.stringify(err));

      // WARN - Getting to this point causes server hangup, so we need to terminate the request
      res.end();
    }
  },
};

function reformatError(err) {
  return pick(err, ['title', 'type', 'status', 'instance', 'detail', 'errors']);
}

function pick(object, keys) {
  const result = {};

  keys.forEach((key) => {
    if (Object.prototype.hasOwnProperty.call(object, key)) {
      result[key] = object[key];
    }
  });

  return result;
}

const terminateHttpServerAndExit = async (error) => {
  // https://betterstack.com/community/guides/logging/how-to-install-setup-and-use-pino-to-log-node-js-applications/#handling-uncaught-exceptions-and-unhandled-promise-rejections
  logger.info('Fatal error encountered, terminating HTTP server');
  logger.fatal(error);

  // maybe implement more complex logic here (like using 'http-terminator' library)
  if (httpServerRef) {
    await httpServerRef.close();
  }
  // https://github.com/goldbergyoni/nodebestpractices/blob/master/sections/errorhandling/shuttingtheprocess.md
  process.exit();
};

const normalizeError = (errorToHandle) => {
  // If the error extends ProblemDetailsError, it's fine
  if (errorToHandle instanceof ProblemDetailsError) {
    return errorToHandle;
  }

  // Transform an API validation error
  // NOTE - There is currently no way of importing the express-openapi-validator error type for comparing
  if (errorToHandle?.errors && errorToHandle.path) {
    // Try to capture express-openapi-validator errors here
    const apiValidationError = new APIValidationError(
      errorToHandle.name,
      errorToHandle.message,
      errorToHandle.status,
      errorToHandle.path,
      // TODO - Normalise each sub-error
      errorToHandle.errors,
    );
    apiValidationError.stack = errorToHandle.stack;
    return apiValidationError;
  }

  // Transform all unidentified Javascript errors to a ProblemDetailsError
  if (errorToHandle instanceof Error) {
    const appError = new ProblemDetailsError(errorToHandle.message, errorToHandle.name, 500);
    appError.stack = errorToHandle.stack;
    return appError;
  }

  // meaning it could be any type,
  const inputType = typeof errorToHandle;
  // TODO - List error types in a constants file
  return new ProblemDetailsError(
    'Error handler received an unexpected error',
    'UnexpectedError',
    500,
    `Error instance with type - ${inputType}, and value - ${util.inspect(errorToHandle)}`,
  );
};

module.exports = {
  errorHandler,
  ProblemDetailsError,
  APIValidationError,
  DatabaseError,
  NotFoundError,
  ForbiddenError,
  ConflictError,
  UnauthorizedError,
  BadRequestError,
  RequestValidationError,
};
