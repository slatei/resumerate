/* eslint-disable max-classes-per-file */
const { config } = require('@resumapi/lib-config');
// TODO - Use http-status library? Or probably better would be a lib-constants

/**
 * Extendable errors class.
 *
 * Credits: https://github.com/bjyoungblood/es6-error/blob/master/src/index.js
 */
class ExtendableError extends Error {
  constructor(message = '') {
    super(message);

    // extending Error is weird and does not propagate `message`
    Object.defineProperty(this, 'message', {
      configurable: true,
      enumerable: false,
      value: message,
      writable: true,
    });

    Object.defineProperty(this, 'name', {
      configurable: true,
      enumerable: false,
      value: this.constructor.name,
      writable: true,
    });

    if (Object.prototype.hasOwnProperty.call(Error, 'captureStackTrace')) {
      Error.captureStackTrace(this, this.constructor);
      return;
    }

    Object.defineProperty(this, 'stack', {
      configurable: true,
      enumerable: false,
      value: new Error(message).stack,
      writable: true,
    });
  }
}

/**
 * Custom ProblemDetails error class
 * Represents a "problem details" object, according to RFC7807
 * https://datatracker.ietf.org/doc/html/rfc7807
 *
 * @class ProblemDetailsError
 * @extends {ExtendableError}
 */
class ProblemDetailsError extends ExtendableError {
  /**
   * Creates an instance of ProblemDetailsError
   *
   * @param {String?} title A short, human-readable summary of the problem type
   * @param {Type?} type A URI reference [RFC3986] that identifies the problem type
   * @param {Number?} status The HTTP status code
   * @param {String?} detail A human-readable explanation specific to this occurrence of the problem
   * @param {String?} instance A URI reference that identifies the specific occurrence of the problem
   * @param {Boolean?} isOperational Whether or not this error is expected by the error handling framework
   * @param {Array[ProblemDetailsError]} errors An array of nested problem details objects
   */
  // eslint-disable-next-line default-param-last
  constructor(title, type, status, detail, isOperational = true, instance, errors) {
    super(title);
    this.title = title;
    this.status = status || 500;
    this.type = `${config.get('api.HOST_URL')}/errors/${type}`;
    this.detail = detail;
    this.instance = instance;
    this.isOperational = isOperational;
    this.errors = errors;
  }
}

class APIValidationError extends ProblemDetailsError {
  constructor(title, detail, status, path, errors) {
    super(title || 'Validation Error', APIValidationError.name, status || 400, detail, true, path, errors);
  }
}

class DatabaseError extends ProblemDetailsError {
  constructor(message) {
    // A database error requires a server restart
    super('Database Error', DatabaseError.name, 500, message, false);
  }
}

class NotFoundError extends ProblemDetailsError {
  constructor(detail, path) {
    super('Not Found', NotFoundError.name, 404, detail, false, path);
  }
}

class ForbiddenError extends ProblemDetailsError {
  constructor(message) {
    super('Forbidden', ForbiddenError.name, 403, message, false);
  }
}

class ConflictError extends ProblemDetailsError {
  constructor(message) {
    super('Conflict', ConflictError.name, 409, message, false);
  }
}

class UnauthorizedError extends ProblemDetailsError {
  constructor(message) {
    super('Unauthorized', UnauthorizedError.name, 401, message, false);
  }
}

class BadRequestError extends ProblemDetailsError {
  constructor(message, title) {
    super(title || 'Bad Request', BadRequestError.name, 400, message, false);
  }
}

class RequestValidationError extends BadRequestError {
  constructor(message) {
    super(message, 'Request validation failed');
  }
}

module.exports = {
  ProblemDetailsError,
  APIValidationError,
  DatabaseError,
  NotFoundError,
  ForbiddenError,
  ConflictError,
  UnauthorizedError,
  BadRequestError,
  RequestValidationError,
};
