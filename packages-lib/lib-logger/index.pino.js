const pino = require('pino');
const { config } = require('@resumapi/lib-config');

const logger = pino({
  transport: config.get('api.LOG_PRETTY_PRINT')
    ? {
        target: 'pino-pretty',
        options: {
          colorize: true,
          sync: true,
        },
      }
    : undefined,
  messageKey: 'message',
});
module.exports = logger;
