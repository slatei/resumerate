module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
    'jest/globals': true,
    mocha: true,
  },
  plugins: ['prettier', 'jest', 'mocha'],
  extends: ['airbnb-base', 'prettier', 'prettier/prettier'],
  overrides: [
    {
      env: {
        node: true,
      },
      files: ['.eslintrc.{js,cjs}'],
      parserOptions: {
        sourceType: 'script',
      },
    },
  ],
  parserOptions: {
    ecmaVersion: 'latest',
  },
  ignorePatterns: ['**/.next/**', '**/.build/.next/**', '**/out/**', '**/_next/**', '**/node_modules/**'],
  rules: {
    'prettier/prettier': 'error',
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: ['**/tests/**', '**/test/**'],
      },
    ],
    'no-console': [
      'error',
      {
        allow: ['time', 'timeEnd'],
      },
    ],
    // https://github.com/goldbergyoni/nodebestpractices/blob/master/sections/errorhandling/returningpromises.md
    'no-return-await': 'off',
    'no-param-reassign': 'off',
    'no-underscore-dangle': 0,
    'no-use-before-define': 'off',
    'import/prefer-default-export': 'off',
  },
  root: true,
};
