const fs = require('fs');
const path = require('path');

const serverCertName = 'server-cert.pem';
const serverKeyName = 'server-key.pem';

const serverCertPath = path.join(__dirname, serverCertName);
const serverKeyPath = path.join(__dirname, serverKeyName);
exports.serverCertPath = serverCertPath;
exports.serverKeyPath = serverKeyPath;

exports.serverCert = fs.readFileSync(serverCertPath);
exports.serverKey = fs.readFileSync(serverKeyPath);
