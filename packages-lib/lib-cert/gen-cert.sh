#!/bin/bash
set -o nounset
set -o errexit

csr_conf="csr.conf"

server_csr="server.csr"
server_key="server-key.pem"
server_cert="server-cert.pem"

openssl genrsa -out "${server_key}" 2048
openssl req -new -key "${server_key}" -config "${csr_conf}" -out "${server_csr}"
openssl x509 -req -in "${server_csr}" -signkey "${server_key}" -out "${server_cert}"
