module.exports = {
  version: '0.2',
  language: 'en,en-GB,lorem',
  dictionaryDefinitions: [
    {
      name: 'default',
      path: './dictionaries/default.dict',
      addWords: true,
      scope: 'default',
    },
    {
      name: 'compounds',
      description: 'Compounded words that, by themselves, would otherwise be spelt correctly',
      path: './dictionaries/compounds.dict',
      addWords: true,
      scope: 'default',
    },
    {
      name: 'tools',
      description: 'Tools, frameworks, standards, libraries, etc.',
      path: './dictionaries/tools.dict',
      addWords: true,
      scope: 'default',
    },
  ],
  dictionaries: ['default', 'compounds', 'tools'],
  useGitignore: true,
  // Used for ignore code blocks in a Markdown document
  patterns: [{ name: 'markdown_code_block', pattern: '/\\s*```[\\s\\S]*?^\\s*```/gm' }],
  languageSettings: [{ languageId: 'markdown', ignoreRegExpList: ['markdown_code_block'] }],
  ignorePaths: [
    // Generic Extensions
    '**/*.{png,jpg,pdf,svg,css}',
    // Folders
    '**/.vscode/**',
    '**/coverage/**',
    '**/dist/**',
    '**/node_modules/**',
    '**/*.fig',
    // Specific files
    '**/{cspell.*,cSpell.*,.cspell.*,cspell.config.*}',
    '**/.gitignore',
    '**/.gcloudignore',
    '**/*postman_*.json',
    '**/.eslintignore',
    '**/.git*',
    '**/jest.config.js',
    '**/.prettierignore',
    '**/.env*',
    '**/package*.json',
    '**/.yarn',
    '**/yarn.lock',
    '**/{LICENCE,LICENSE}',
    '**/CHANGELOG.md',
    '**/TODO.md',
  ],
  ignoreRegExpList: [
    '/https?%3A%2F%2F(.*)/', // URI encoded strings
    '/[^s]*.apps.googleusercontent.com[^s]*/', // Dynamic google urls
    '/mongodb\\+srv:[^\\s]*/', // Mongo database connection strings
  ],
};
