const mongoose = require('mongoose');
const logger = require('@resumapi/lib-logger');
const { config } = require('@resumapi/lib-config');
const { DatabaseError } = require('@resumapi/lib-error');

async function connect() {
  try {
    logger.info({ databaseUri: config.get('db.MONGODB_HOST') }, 'Connecting to database...');

    const dbUser = config.get('db.MONGODB_USERNAME');
    const dbPassword = config.get('db.MONGODB_PASSWORD');

    await mongoose.connect(config.get('db.MONGODB_HOST'), {
      dbName: config.get('db.MONGODB_COLLECTION_NAME'),
      ...(dbUser && { user: dbUser }),
      ...(dbPassword && { pass: dbPassword }),
    });
    logger.info('Database connection established');
  } catch (error) {
    logger.error(error, 'Error connecting to MongoDB');
    throw new DatabaseError(error.message);
  }
}

module.exports = { connect };
