# Integration Tests

## Folder Structure

A well-organized folder structure for API integration tests can greatly improve maintainability and readability. Here's a suggested folder structure for organizing API integration tests:

```tree
test-integration/
├── api/
│   ├── resume.test.js     // API integration tests for the 'resume' endpoints
│   └── user.test.js       // API integration tests for the 'user' endpoints (if applicable)
│
├── common/
│   ├── helpers/
│   │   └── axios.js       // Custom Axios instance with test-specific configurations
│   │
│   └── fixtures/
│       └── test-data.js   // Test data and fixtures for API requests and responses
│
├── setup/
│   └── server.js          // Start and stop the test server (if necessary)
│
└── shared/
    └── config.js          // Shared test configuration (e.g., API base URL)
```

Here's an explanation of each folder and file within the structure:

- **integration/**: This is the main folder for your API integration tests. Each API endpoint or feature should have its own test file(s) within this folder. For example, you might have separate test files for testing the 'resume' and 'user' endpoints.

  - **api/**: This subfolder organizes your API endpoint-specific test files.

    - **resume.test.js**: A test file for the 'resume' API endpoints.
    - **user.test.js**: A test file for the 'user' API endpoints (if applicable).

  - **common/**: This subfolder contains common utilities and configurations that can be shared among different test files.

    - **helpers/**: Custom test helpers or utilities. For example, a custom Axios instance with test-specific configurations.

    - **fixtures/**: Test data and fixtures that are used in your API requests and responses. This can include sample JSON payloads, response templates, and other data.

- **setup/**: This folder contains setup files and utilities for your tests.

  - **server.js**: A script to start and stop the test server (if necessary). This file may not be needed if your API server is started and stopped differently.

- **shared/**: This folder contains shared configurations and utilities that can be used across different types of tests (e.g., unit tests, integration tests).

  - **config.js**: Shared test configuration, such as the API base URL, that can be imported and used in various test files.

By organizing integration tests in this way, they are kept modular and maintainable. Each API endpoint has its own test file, and common utilities and data are stored separately for easy reuse. Additionally, the 'common' folder allows centralized shared test-related code.

## Use Mocha Root Hooks to Perform Global Setup

https://mochajs.org/#root-hook-plugins

## Links

- https://www.chaijs.com/plugins/chai-as-promised/
- https://dev.to/ali_adeku/guide-to-writing-integration-tests-in-express-js-with-jest-and-supertest-1059#:~:text=Setting%20up%20your%20Test%20Folder,all%20your%20integration%20test%20files.
- https://mochajs.org/
