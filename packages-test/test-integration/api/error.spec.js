const axios = require('../common/helpers/axios');

// {
//   "title": "Not Found",
//   "type": "https://localhost:8088/errors/NotFoundError",
//   "status": 404,
//   "instance": "/v1/auth/registers",
//   "detail": "Route not recognised"
// }
const ErrorSchema = {
  type: 'object',
  properties: {
    title: { type: 'string' },
    type: { type: 'string' },
    status: { type: 'number' },
    instance: { type: 'string' },
    detail: { type: 'string' },
  },
};

describe('Test valid error responses', async () => {
  it('should handle 404 not found gracefully', async () => {
    const response = await axios.get('/nonexistent-endpoint');

    expect(response.status).to.equal(404);
    expect(response.data).to.be.jsonSchema(ErrorSchema);
  });
});
