// const axios = require('axios');
const axios = require('../common/helpers/axios'); // Import the custom Axios instance

describe('GET /resume', () => {
  // Define the API endpoint URL
  const apiUrl = '/resume';

  before(async () => {
    // Perform any setup before running the tests, if needed
    // This is where you can start your API server or perform other setup steps
  });

  after(async () => {
    // Perform any cleanup after running the tests, if needed
    // This is where you can stop your API server or perform other cleanup steps
  });

  it.skip('should retrieve the resume successfully', async () => {
    // Arrange: Define any necessary test data or setup

    // Act: Make an HTTP GET request to the API endpoint
    const response = await axios.get(apiUrl);

    // Assert: Verify the response and expectations
    expect(response.status).to.equal(200);
    expect(response.data).to.be.an('object');
  });

  it('should return a 401 status when the endpoint is called without authentication');

  it('should return a 403 status when the endpoint is called with insufficient permissions');

  it('should return a 429 status when the endpoint is called too frequently (rate limiting)');

  it('should return a 500 status when the server encounters an internal error');
});
