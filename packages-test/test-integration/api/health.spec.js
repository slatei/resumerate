const { expect } = require('chai');
const http = require('../common/helpers/axios');

describe('GET /health', () => {
  const apiUrl = '/v1/health';

  before(async () => {
    // Perform any setup before running the tests, if needed
    // This is where you can start your API server or perform other setup steps
  });

  after(async () => {
    // Perform any cleanup after running the tests, if needed
    // This is where you can stop your API server or perform other cleanup steps
  });

  it('should retrieve a ready status from the health endpoint successfully', async () => {
    // Arrange: Define any necessary test data or setup

    // Act: Make an HTTP GET request to the API endpoint
    const response = await http.get(apiUrl);

    // Assert: Verify the response and expectations
    expect(response.status).to.equal(200);
    expect(response.data).to.be.an('object'); // Assumes the response is a JSON object
    expect(response.data.status).to.equal('ready');
  });
});
