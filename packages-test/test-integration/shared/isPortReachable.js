const net = require('net');

module.exports = async (port) => {
  const client = net.createConnection({ port });

  client.on('connect', () => {
    client.end();
    return true;
  });

  // eslint-disable-next-line no-unused-vars
  client.on('error', (err) => {
    return false;
  });
};
