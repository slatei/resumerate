# Integration Test Suite Plan

- Start Server Setup Script
- Start Database Setup Script

global-setup:

```text
if(production)
  skip setup
else
  if (server not started) -> Start server on dedicated test port?
    start server
  if (database not started) -> Start database on dedicated test port?
    start database
```

global-teardown

stop database
stop server
