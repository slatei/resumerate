// https://github.com/chaijs/chai#pre-native-modules-usage-registers-the-chai-testing-style-globally
// https://github.com/chaijs/chai/issues/769
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const chaiJsonSchema = require('chai-json-schema');

chai.use(chaiAsPromised);
chai.use(chaiJsonSchema);

global.expect = chai.expect;

module.exports = chai;
