/* eslint-disable no-console */
// Start a MongoDB instance if not already running

const path = require('path');
const dockerCompose = require('docker-compose');
const isPortReachable = require('../shared/isPortReachable');

// Root hook.
exports.mochaHooks = {
  beforeEach(done) {
    console.log('database.beforeEach');
    done();
  },
};

// Bonus: global fixture, runs once before everything.
exports.mochaGlobalSetup = async () => {
  // console.log('mochaGlobalSetup');
  console.time('database-setup');

  // ️️️✅ Best Practice: Speed up during development, if already live then do nothing
  const isDBReachable = await isPortReachable(27097);
  if (!isDBReachable) {
    // ️️️✅ Best Practice: Start the infrastructure within a test hook - No failures occur because the DB is down
    await dockerCompose.upAll({
      cwd: path.join(__dirname),
      log: true,
    });

    // TODO - Check that the database is up and running
    // await dockerCompose.exec('database', ['sh', '-c', 'until pg_isready ; do sleep 1; done'], {
    //   cwd: path.join(__dirname),
    // });
  }

  console.timeEnd('database-setup');
};

exports.mochaGlobalTeardown = async () => {
  if (process.env.CI) {
    dockerCompose.down();
  }
};
