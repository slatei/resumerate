const axios = require('axios');
const https = require('https');
const { config } = require('@resumapi/lib-config');

const BASE_URL = config.get('api.HOST_URL');

// Create a custom Axios instance for API integration tests
const createTestAxiosInstance = () => {
  // Configure an HTTPS Agent that trusts self-signed certificates
  const httpsAgent = new https.Agent({ rejectUnauthorized: false });

  // Create the custom Axios instance with your preferred configurations
  const axiosInstance = axios.create({
    baseURL: BASE_URL,
    httpsAgent,
    timeout: 10000,
    // Don't reject the Promise if the API returns an error
    validateStatus: (status) => status >= 200 && status <= 503,
  });

  // Interceptors, defaults, or other custom configurations can be added here

  return axiosInstance;
};

module.exports = createTestAxiosInstance();
