const { config } = require('@resumapi/lib-config');
const path = require('path');

('use strict');

// This is a JavaScript-based config file containing every Mocha option plus others.
// If you need conditional logic, you might want to use this type of config,
// e.g. set options via environment variables 'process.env'.
// Otherwise, JSON or YAML is recommended.

// https://mochajs.org/#configuring-mocha-nodejs

// All mocha setup files required regardless of test environment
const globalRequires = ['setup/chai.js'];
// Additional mocha requires files for testing in specific environments
// For example: Start the database and server if testing locally
const testSetupRequires = config.get('env') === 'test' ? ['setup/database.js', 'setup/server.js'] : [];
// Expand the requires array to absolute paths for running tests from different working directories
const allRequires = [...globalRequires, ...testSetupRequires]
  .map((relativePath) => path.join(__dirname, relativePath))
  .join(', ');

module.exports = {
  // 'allow-uncaught': false,
  // 'async-only': false,
  // bail: false,
  // 'check-leaks': false,
  // color: true,
  // delay: false,
  // diff: true,
  // exit: false, // could be expressed as "'no-exit': true"
  // extension: ['js', 'cjs', 'mjs'],
  // 'fail-zero': true,
  // fgrep: 'something', // fgrep and grep are mutually exclusive
  // file: ['/path/to/some/file', '/path/to/some/other/file'],
  // 'forbid-only': false,
  // 'forbid-pending': false,
  // 'full-trace': false,
  // global: ['jQuery', '$'],
  // grep: /something/i, // also 'something', fgrep and grep are mutually exclusive
  // growl: false,

  /* We need to ignore files when running wildcard globs at root level */
  ignore: ['**/node_modules/**/*'],

  // 'inline-diffs': false,
  // // invert: false, // needs to be used with grep or fgrep
  // jobs: 1,
  // 'node-option': ['unhandled-rejections=strict'], // without leading "--", also V8 flags
  // package: './package.json',
  // parallel: false,
  recursive: true,
  // reporter: 'spec',
  // 'reporter-option': ['foo=bar', 'baz=quux'], // array, not object
  require: allRequires,

  // retries: 1,
  // slow: '75',
  // sort: false,

  /* Avoid setting this, otherwise we can't run specific test files */
  // spec: '**/*.spec.js',

  timeout: 2000,

  // // timeout: false, // same as "timeout: 0"
  // 'trace-warnings': true, // node flags ok
  // ui: 'bdd',
  // 'v8-stack-trace-limit': 100, // V8 flags are prepended with "v8-"
  // watch: false,
  // 'watch-files': ['lib/**/*.js', 'test/**/*.js'],
  // 'watch-ignore': ['lib/vendor'],
};
