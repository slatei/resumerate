# VSCode REST HTTP Client

Use the VSCode [REST API client](https://github.com/Huachao/vscode-restclient) to test endpoints quickly within the vscode editor

<!-- spellchecker:words humao -->

```shell
code --install-extension humao.rest-client
```
