const newman = require('newman');
const collection = require('./collections/ResumAPI.postman_collection.json');

newman.run(
  {
    collection,
    reporters: 'cli',
    insecure: true,
    bail: true,
  },
  (err) => {
    if (err) {
      throw err;
    }
    // eslint-disable-next-line no-console
    console.log('Collection run complete!');
  },
);
