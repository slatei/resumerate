/* eslint-disable no-console */

/**
 * Postman collection formatter script.
 * Iterates through all the project postman collection and environment files, runs an eslint
 * formatter against postman scripts, and will normalise the json file for pull requests
 *
 * https://schema.postman.com/collection/json/v2.1.0/draft-04/collection.json
 */

// TODO - Add CLI input, to allow integration with husky
// TODO - Add "fix" flag for optionally applying formatting

const fs = require('fs');
const os = require('os');
const path = require('path');
const { ESLint } = require('eslint');
const prettier = require('prettier');

const COLLECTIONS_DIR = 'collections';
const IGNORE_FILES = process.env.IGNORE_FILES || [];

// List of dynamically generated postman keys to remove, for cleaner diff comparisons
const PostmanKeysToDelete = ['_', 'id', '_postman_id', '_postman_exported_at', '_postman_exported_using'];

const lintConfig = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: ['airbnb-base'],
  parserOptions: {
    ecmaVersion: 12,
  },
  rules: {
    'no-console': 'off',
    'import/no-unresolved': 'off',
    'import/no-extraneous-dependencies': 'off',
    'no-eval': 'off',
    'max-len': 'off',
    'no-underscore-dangle': 'off',
    'no-unused-expressions': 'off',
    'operator-linebreak': 'off',
  },
  globals: {
    _: 'readonly',
    atob: 'readonly',
    globals: 'readonly',
    KJUR: 'readonly',
    pm: 'readonly',
    postman: 'readonly',
    tests: 'readonly',
    environment: 'readonly',
    CryptoJS: 'readonly',
    cheerio: 'readonly',
  },
};

const prettierConfig = {
  parser: 'babel',
  filepath: '',
  trailingComma: 'all',
  singleQuote: true,
  tabWidth: 2,
  semi: true,
};

const eslint = new ESLint({
  fix: true,
  useEslintrc: false,
  overrideConfig: !process.env.DEBUG ? lintConfig : null,
});

/**
 * Recursively walks directory and returns a list of all files found
 *
 * @param {String} path Full path to directory to search
 * @returns {Array[String]} List of
 */
async function walkDir(dir) {
  const files = await fs.promises.readdir(dir);
  const fileStats = await Promise.all(
    files.map(async (filename) => {
      const file = path.resolve(dir, filename);
      const stat = await fs.promises.lstat(file);
      if (stat.isDirectory()) {
        return walkDir(file);
      }
      return [file];
    }),
  );
  return fileStats.reduce((acc, val) => acc.concat(val), []);
}

/**
 * Runs the eslint formatter against a supplied postman script
 * Note: The prettier formatter is applied after 'eslint --fix' to avoid rule collisions
 *
 * @param {*} event Event postman-collection Event: https://www.postmanlabs.com/postman-collection/Event.html
 */
async function formatScript(event, parent) {
  const { exec } = event?.script || {};
  if (exec) {
    const script = Array.isArray(exec) ? exec.join('\n') : exec;
    const results = await eslint.lintText(script);

    await Promise.all(
      results.map(async (entry) => {
        const formattedResult = await prettier.format(entry.output ? entry.output : script, prettierConfig);
        event.script.exec = formattedResult.split('\n');

        entry.filePath = `'${parent}:${event.listen}'`;
      }),
    );

    return results;
  }
  return null;
}

/**
 * Recursively iterate through all requests and return a list of formatted event scripts
 * @param {*} item Can be any of Postman ItemGroup, Item, or EventList
 */
async function formatAllItems(item) {
  const allEvents = [];

  if (item.item) {
    const events = await Promise.all(item.item.map((request) => formatAllItems(request)));
    allEvents.push(...events.flat(Infinity));
  }

  if (item.event) {
    const itemName = item?.name || item?.info?.name;
    // Event is being modified
    const events = await Promise.all(item.event.map((event) => formatScript(event, itemName)));
    allEvents.push(...events.flat(Infinity));
  }

  return allEvents;
}

/**
 * Recursively walk object and remove object keys that belong to an array of provided keys
 * @param {Object} content Object to remove keys from
 * @param {Array} keys List of keys to remove from the provided content
 */
function removeKeysFromDictionary(content, keys) {
  try {
    if (Array.isArray(content)) {
      content.forEach((item) => {
        removeKeysFromDictionary(item, keys);
      });
    } else if (typeof content === 'object' && content !== null) {
      Object.getOwnPropertyNames(content).forEach((key) => {
        // eslint-disable-next-line no-param-reassign
        if (keys.indexOf(key) !== -1) delete content[key];
        else removeKeysFromDictionary(content[key], keys);
      });
    }
  } catch (error) {
    console.log(`Unable to remove keys '${keys}' from dictionary:\n${JSON.stringify(content)}`);
    console.log(error);
  }
}
/**
 * Removes items from a list of objects with keys that start with a provided prefix string
 *
 * Usage:
 *    filterVariablesByPrefix([{"key": "t_var1", ...}, {"key": "c_var2", ...}], "t_")
 * Output:
 *  [{"key": "c_var2", ...}]    (Noting that variable prefixed with "t_" was removed from the list)
 *
 * @param {Array} variables Javascript object that contains key:value pairs
 * @param {String} prefix Prefix to search for when stripping "key" elements from "variables" object
 */
function filterVariablesByPrefix(variables, prefix) {
  return variables.filter((variable) => !variable.key.startsWith(prefix));
}

async function main() {
  try {
    const postmanFiles = await walkDir(COLLECTIONS_DIR);
    const formatter = await eslint.loadFormatter();

    // Format environment and collection files
    postmanFiles
      .filter(
        (file) =>
          file.endsWith('postman_environment.json') ||
          file.endsWith('postman_globals.json') ||
          file.endsWith('postman_collection.json'),
      )
      .filter((file) => !IGNORE_FILES.includes(path.parse(file).base))
      .forEach(async (postmanFile) => {
        const postmanFileConfig = JSON.parse(fs.readFileSync(postmanFile));

        const isCollection = postmanFile.endsWith('postman_collection.json');

        // Strip out any "temporary" variables that are prefixed with 't_'
        if (!isCollection || isCollection.variable) {
          const filteredVariables = filterVariablesByPrefix(postmanFileConfig.values, 't_');
          postmanFileConfig.values = filteredVariables;
        }

        // Run formatter over all of the collection scripts
        if (isCollection) {
          const results = await formatAllItems(postmanFileConfig);
          results.forEach((result) => {
            if (result?.errorCount > 0 || result?.warningCount > 0) {
              console.info(`${postmanFile}\n${formatter.format([result])}`);
            }
          });
        }

        // Remove redundant metadata from exported Postman file, and save
        removeKeysFromDictionary(postmanFileConfig, PostmanKeysToDelete);

        fs.writeFileSync(postmanFile, JSON.stringify(postmanFileConfig, null, 2) + os.EOL);
      });
  } catch (error) {
    throw Error(error);
  }
}

main().catch((error) => console.error(error));
