const catchAsync = require('../utils/catchAsync');
const { themeService } = require('../services');

const getResume = catchAsync(async (req, res) => {
  const { userId, theme } = req.query;

  const resumeHtml = await themeService.getRenderedResume(userId, theme);
  res.send(resumeHtml);
});

module.exports = {
  getResume,
};
