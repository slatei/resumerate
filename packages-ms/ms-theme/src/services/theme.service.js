const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const axios = require('axios');
const escalade = require('escalade');
const fastGlob = require('fast-glob');
const httpStatus = require('http-status');
const config = require('../config/config');
const ApiError = require('../utils/ApiError');

const isDirectory = (dir) => {
  try {
    return fs.lstatSync(dir).isDirectory();
  } catch {
    return false;
  }
};

/**
 * Retrieve a list of all installed jsonresume-themes in the node_modules dir
 * @param {string} theme
 * @param {string} nodeModulesDir
 * @returns
 */
const findThemesInNodeModules = async (nodeModulesDir, theme = '*') => {
  return (
    await fastGlob(
      [
        `jsonresume-theme-${theme}/package.json`,
        `@*/jsonresume-theme-${theme}/package.json`,
        `@jsonresume/theme-${theme}/package.json`,
      ],
      {
        cwd: nodeModulesDir,
      },
    )
  ).map(path.dirname);
};

/**
 * Loads the available jsonresume-themes and resolves the associated npm module paths
 * If no theme is provided, returns the first theme found in node_modules dir
 * @param {string} theme
 */
const loadThemes = async (theme) => {
  // Find the node_modules directory
  const themeSearchDir = await escalade(process.cwd(), (_dir, names) => names.includes('node_modules') && '.');
  if (!themeSearchDir) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Unable to find node_modules directory');
  }
  const nodeModulesDir = path.resolve(themeSearchDir, 'node_modules');

  // In some fringe cases (e.g.: files "mounted" as virtual directories), the
  // isDirectory(themeSearchDir) check might be false even though the node_modules actually exists.
  if (!isDirectory(nodeModulesDir) && !isDirectory(themeSearchDir)) {
    throw new ApiError(
      httpStatus.INTERNAL_SERVER_ERROR,
      `Directory '${themeSearchDir}' does not exist or is not a directory`,
    );
  }

  // Get the list of all potential theme npm modules, resolving the path to the associated node_module
  const themesInfo = (await findThemesInNodeModules(nodeModulesDir, theme)).map((name) => ({
    name: name.split('-').pop(),
    path: require.resolve(name, { paths: [themeSearchDir] }),
  }));
  if (!themesInfo || themesInfo.length === 0) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, `No themes found loaded in node_modules directory`);
  }

  // Filter out duplicates
  return _.uniqBy(themesInfo, 'path').map((info) => ({
    ...info,
    // eslint-disable-next-line global-require, import/no-dynamic-require
    module: require(info.path),
  }));
};

/**
 * Load resume from backend server and apply jsonresume-theme, returning rendered html content
 * @param {Object} Resume
 * @returns {Promise<Resume>}
 */
const getRenderedResume = async (userId, theme) => {
  // eslint-disable-next-line no-unused-vars
  const [loadedTheme, ...otherThemes] = await loadThemes(theme);

  const { data: resume } = await axios.get(`${config.server.host}/resume/${config.userId}`);

  return await loadedTheme.module.render(resume);
};

module.exports = {
  getRenderedResume,
};
