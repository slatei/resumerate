const path = require('path');
const express = require('express');
const OpenApiValidator = require('express-openapi-validator');
const { themeController } = require('../../controllers');

const router = express.Router();

// Openapi validation
router.use(
  OpenApiValidator.middleware({
    apiSpec: path.join(__dirname, '../..', 'docs', 'resume-theme.yml'),
    validateRequests: true,
    validateResponses: true,
  }),
);

router.route('/').get(themeController.getResume);

module.exports = router;
