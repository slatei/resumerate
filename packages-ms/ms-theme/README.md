# Dynamically apply themes to json file retrieved from API backend

## Behaviour

- Install all resume themes listed in registry https://registry.jsonresume.org/themes
- Create service for dynamically loading themes installed with npm
- Query the registry for new themes that haven't been synchronised
  - Dynamically install themes?

### Route Logic

```text
GET /resume?theme=<theme>&user=<user>

1. Use authorization header bearer token to make backend request to resume-api
2. (Asynchronously) load theme specified by `theme`
3. Render resume json content using theme.render method
4. Return rendered html
```

## Links

### Load themes from node_modules dir

- https://github.com/rbardini/resumed/blob/master/src/load-themes.ts
- https://github.com/prettier/prettier/blob/main/src/common/load-plugins.js
