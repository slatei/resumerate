const logger = require('@resumapi/lib-logger');
const { config } = require('@resumapi/lib-config');
const { startHttpServer } = require('@resumapi/lib-broker');
const { serverCert, serverKey } = require('@resumapi/lib-cert');
const app = require('./index');

// Start the service locally, independent of other services
startHttpServer({
  app,
  name: 'ms-resume',
  port: config.get('api.PORT'),
  logger,
  basePath: '/',
  ...(config.get('api.SSL_ENABLED')
    ? {
        sslCert: serverCert,
        sslKey: serverKey,
      }
    : {}),
});
