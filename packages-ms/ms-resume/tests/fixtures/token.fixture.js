const moment = require('moment');
const { config } = require('@resumapi/lib-config');
const { tokenTypes } = require('../../service/config/tokens');
const tokenService = require('../../service/services/token.service');
const { userOne, admin } = require('./user.fixture');

const accessTokenExpires = moment().add(config.get('auth.JWT_ACCESS_EXPIRATION_MINUTES'), 'minutes');
const userOneAccessToken = tokenService.generateToken(userOne._id, accessTokenExpires, tokenTypes.ACCESS);
const adminAccessToken = tokenService.generateToken(admin._id, accessTokenExpires, tokenTypes.ACCESS);

module.exports = {
  userOneAccessToken,
  adminAccessToken,
};
