const mongoose = require('mongoose');
const { config } = require('@resumapi/lib-config');

const setupTestDB = () => {
  beforeAll(async () => {
    await mongoose.connect(`${config.get('db.MONGODB_HOST')}/${config.get('db.MONGODB_DATABASE_NAME')}`);
  });

  beforeEach(async () => {
    await Promise.all(
      Object.values(mongoose.connection.collections).map(async (collection) => collection.deleteMany()),
    );
  });

  afterAll(async () => {
    await mongoose.disconnect();
  });
};

module.exports = setupTestDB;
