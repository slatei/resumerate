const allRoles = {
  user: ['getResume', 'manageResume', 'readResource', 'writeResource'],
  admin: ['getUsers', 'manageUsers', 'getResume', 'manageResume', 'readResource', 'writeResource'],
};

const roles = Object.keys(allRoles);
const roleRights = new Map(Object.entries(allRoles));

module.exports = {
  roles,
  roleRights,
};
