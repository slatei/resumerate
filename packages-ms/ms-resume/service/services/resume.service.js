const { Resume } = require('../models');

/**
 * Create a resume
 * @param {Object} resumeBody
 * @returns {Promise<Resume>}
 */
const createResume = async (userId, resumeBody) => {
  return Resume.create({ user: userId, ...resumeBody });
};

/**
 * Get first resume for specific user
 * @param {Object} Resume
 * @returns {Promise<Resume>}
 */
const getResume = async (userId) => {
  return Resume.find({ user: userId });
};

/**
 * Update resume by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<Resume>}
 */
const updateResume = async (userId, body) => {
  return Resume.findOneAndUpdate({ user: userId }, { ...body, user: userId }).setOptions({
    overwrite: true,
    new: true,
    useFindAndModify: false,
  });
};

/**
 * Delete users resume
 * @param {ObjectId} userId
 */
const deleteResume = async (userId) => {
  return Resume.deleteMany({ user: userId });
};

module.exports = {
  createResume,
  getResume,
  updateResume,
  deleteResume,
};
