const { Types } = require('mongoose');
const { ConflictError } = require('@resumapi/lib-error');
const { Resume } = require('../models');

/**
 * Create a resource
 * https://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate
 * @param {ObjectId} userId
 * @param {String} resourceName
 * @param {Object} body
 * @returns {Promise<Object>}
 */
const createResource = async (userId, resourceName, body) => {
  if (resourceName === 'basics') {
    const resumeExists = await Resume.findOne({ user: userId });
    if (resumeExists && resumeExists.basics) {
      throw new ConflictError("Resume already has 'basics' section");
    }

    const resume = await Resume.findOneAndUpdate(
      { user: userId },
      { $set: { basics: body } },
      { upsert: true, new: true },
    );
    return resume.basics;
  }
  const resourceId = new Types.ObjectId();
  const resume = await Resume.findOneAndUpdate(
    { user: userId },
    { $push: { [resourceName]: { _id: resourceId, ...body } } },
    { upsert: true, new: true },
  );
  return resume[resourceName].id(resourceId).toJSON();
};

/**
 * Get the resume sub-document matching a provided resource name
 * @param {ObjectId} userId
 * @param {String} resourceName
 * @returns {Promise<Object>}
 */
const getResources = async (userId, resourceName) => {
  const resume = await Resume.findOne({ user: userId });

  const resources = resume[resourceName];
  if (Array.isArray(resources)) {
    return resources.map((resource) => resource.toJSON());
  }
  return resources.toJSON();
};

/**
 * Get resource from resume sub-document array by id
 * @param {ObjectId} userId
 * @param {String} resourceName
 * @param {ObjectId} resourceId
 * @returns {Promise<Object>}
 */
const getResourceById = async (userId, resourceName, resourceId) => {
  const resume = await Resume.findOne({ user: userId });

  return resume[resourceName]?.id(resourceId)?.toJSON();
};

/**
 * Update resource by id
 * Resources:
 *  - https://stackoverflow.com/questions/26156687/mongoose-find-update-subdocument
 * @param {ObjectId} userId
 * @param {String} resourceName
 * @param {ObjectId} resourceId
 * @param {Object} updateBody
 * @returns {Object}
 */
const updateResourceById = async (userId, resourceName, resourceId, updateBody) => {
  let resource;
  if (resourceName === 'basics') {
    const resume = await Resume.findOneAndUpdate({ user: userId }, { $set: { basics: updateBody } }, { new: true });
    resource = resume[resourceName]?.toJSON();
  } else {
    const queryString = `${[resourceName]}._id`;
    const positionalOperatorString = `${[resourceName]}.$`;

    const resume = await Resume.findOneAndUpdate(
      { user: userId, [queryString]: resourceId },
      { $set: { [positionalOperatorString]: { _id: resourceId, ...updateBody } } },
      { new: true },
    );

    resource = resume[resourceName]?.id(resourceId)?.toJSON();
  }

  return resource;
};

/**
 * Delete resource by id
 * @param {ObjectId} userId
 * @param {String} resourceName
 * @param {ObjectId} resourceId
 */
const deleteResourceById = async (userId, resourceName, resourceId) => {
  const resume = await Resume.findOne({ user: userId });
  resume[resourceName].id(resourceId)?.deleteOne();
  await resume.save();
};

module.exports = {
  getResources,
  createResource,
  getResourceById,
  updateResourceById,
  deleteResourceById,
};
