const nodemailer = require('nodemailer');
const { config } = require('@resumapi/lib-config');
const logger = require('@resumapi/lib-logger');

const transportOptions = {
  host: config.get('email.SMTP_HOST'),
  port: config.get('email.SMTP_PORT'),
  auth: {
    user: config.get('email.SMTP_USERNAME'),
    pass: config.get('email.SMTP_PASSWORD'),
  },
};
logger.debug(JSON.stringify(transportOptions));
const transport = nodemailer.createTransport(transportOptions);
/* istanbul ignore next */
if (config.get('env') !== 'test') {
  transport
    .verify()
    .then(() => logger.info('Connected to email server'))
    .catch((err) =>
      logger.warn('Unable to connect to email server. Make sure you have configured the SMTP options in .env', err),
    );
}

/**
 * Send an email
 * @param {string} to
 * @param {string} subject
 * @param {string} text
 * @returns {Promise}
 */
const sendEmail = async (to, subject, html) => {
  const msg = { from: config.get('email.FROM_ADDRESS'), to, subject, html };
  await transport.sendMail(msg);
};

/**
 * Send reset password email
 * @param {string} to
 * @param {string} token
 * @returns {Promise}
 */
const sendResetPasswordEmail = async (to, token) => {
  const subject = 'Reset password';
  // replace this url with the link to the reset password page of your front-end app
  const resetPasswordUrl = `${config.get('api.HOST_URL')}/v1/auth/reset-password?token=${token}`;
  const html = `
<p>Dear user,</p>
<p>To reset your password, click on this <a href="${resetPasswordUrl}">link</a></p>
<p>If you did not request any password resets, then please ignore this email.</p>`;
  await sendEmail(to, subject, html);
};

/**
 * Send verification email
 * @param {string} to
 * @param {string} token
 * @returns {Promise}
 */
const sendVerificationEmail = async (to, token) => {
  const subject = 'Email Verification';
  // replace this url with the link to the email verification page of your front-end app
  const verificationEmailUrl = `${config.get('api.HOST_URL')}/v1/auth/verify-email?token=${token}`;
  const html = `
<h3>Thankyou for registering with Thorpincorp json-resume!</h3>
<p>Please click on this <a target="_blank" rel="noopener noreferrer" href="${verificationEmailUrl}">link</a> to verify your email</p>
<p>If you did not create an account, then please ignore this email.</p>
`;
  await sendEmail(to, subject, html);
};

module.exports = {
  transport,
  sendEmail,
  sendResetPasswordEmail,
  sendVerificationEmail,
};
