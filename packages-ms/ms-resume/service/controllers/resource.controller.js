const httpStatus = require('http-status');
const { NotFoundError } = require('@resumapi/lib-error');
const catchAsync = require('../utils/catchAsync');
// TODO - replace resource service with some sort of database adapter
const { resourceService } = require('../services');

const getResources = catchAsync(async (req, res) => {
  const { user, params } = req;

  const resources = await resourceService.getResources(user._id, params.resource);

  res.status(httpStatus.OK).json(resources);
});

const createResource = catchAsync(async (req, res) => {
  const { user, body, params } = req;

  const resource = await resourceService.createResource(user._id, params.resource, body);
  res.status(httpStatus.CREATED).json(resource);
});

const getResource = catchAsync(async (req, res) => {
  const { user, params } = req;

  const resource = await resourceService.getResourceById(user._id, params.resource, params.id);
  if (!resource) {
    throw new NotFoundError('Resource with given id does not exist', req.path);
  }

  res.status(httpStatus.OK).json(resource);
});

const updateResource = catchAsync(async (req, res) => {
  const { user, params, body, path } = req;

  if (path === '/basics') {
    params.resource = 'basics';
    params.id = null;
  }

  const resource = await resourceService.updateResourceById(user._id, params.resource, params.id, body);

  res.status(httpStatus.OK).json(resource);
});

const deleteResource = catchAsync(async (req, res) => {
  const { user, params } = req;

  await resourceService.deleteResourceById(user._id, params.resource, params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createResource,
  getResources,
  getResource,
  updateResource,
  deleteResource,
};
