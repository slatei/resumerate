module.exports.authController = require('./auth.controller');
module.exports.userController = require('./user.controller');
module.exports.resumeController = require('./resume.controller');
module.exports.resourceController = require('./resource.controller');
