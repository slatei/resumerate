const httpStatus = require('http-status');
const { ConflictError, NotFoundError } = require('@resumapi/lib-error');
const catchAsync = require('../utils/catchAsync');
const { resumeService } = require('../services');

const removeKey = (obj, keyToRemove) =>
  JSON.parse(JSON.stringify(obj, (key, val) => (key === keyToRemove ? undefined : val)));

// Walk an object, and remove any empty array objects
const removeEmptyArrays = (obj) =>
  Object.keys(obj)
    .filter((key) => !Array.isArray(obj[key]) || (Array.isArray(obj[key]) && obj[key].length !== 0))
    .reduce((acc, key) => {
      acc[key] = obj[key];
      return acc;
    }, {});

const createResume = catchAsync(async (req, res) => {
  const { user, body } = req;

  const resumeExists = (await resumeService.getResume(user._id)).length > 0;
  if (resumeExists) {
    throw new ConflictError('User already has a resume');
  }

  try {
    const resume = await resumeService.createResume(user._id, body);
    res.status(httpStatus.CREATED).json(removeKey(removeEmptyArrays(resume.toJSON()), 'id'));
  } catch (error) {
    await resumeService.deleteResume(user._id);
    throw error;
  }
});

const getResume = catchAsync(async (req, res) => {
  const { user } = req;

  const [resume] = await resumeService.getResume(user._id);
  if (!resume) {
    return res.status(httpStatus.NOT_FOUND).send();
  }

  return res.status(httpStatus.OK).json(removeKey(removeEmptyArrays(resume.toJSON()), 'id'));
});

const getResumeForUser = catchAsync(async (req, res) => {
  const { params } = req;

  try {
    const [resume] = await resumeService.getResume(params.user);
    if (!resume) {
      return res.status(httpStatus.NOT_FOUND).send();
    }

    return res.status(httpStatus.OK).json(removeKey(removeEmptyArrays(resume.toJSON()), 'id'));
  } catch (error) {
    return res.status(httpStatus.NOT_FOUND).send();
  }
});

const updateResume = catchAsync(async (req, res) => {
  const { user, body } = req;

  const resume = await resumeService.updateResume(user._id, body);
  if (!resume) {
    throw new NotFoundError('No resume found for user', req.path);
  }

  res.status(httpStatus.OK).json(removeKey(removeEmptyArrays(resume.toJSON()), 'id'));
});

const deleteResume = catchAsync(async (req, res) => {
  const { user } = req;

  // try {
  await resumeService.deleteResume(user._id);
  // } catch (error) {
  //   throw new Error(httpStatus.INTERNAL_SERVER_ERROR, error);
  // }

  return res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  getResume,
  createResume,
  updateResume,
  deleteResume,
  getResumeForUser,
};
