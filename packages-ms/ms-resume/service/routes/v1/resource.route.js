const express = require('express');
const { ResumeValidator } = require('@resumapi/lib-openapi');
const auth = require('../../middlewares/auth');
const userEmailIsVerified = require('../../middlewares/userEmailIsVerified');
const { resourceController } = require('../../controllers');

const router = express.Router();

router.use(ResumeValidator);

router
  .route('/:resource')
  .get(auth('readResource'), userEmailIsVerified, resourceController.getResources)
  .post(auth('writeResource'), userEmailIsVerified, resourceController.createResource);

router.route('/basics').put(auth('writeResource'), userEmailIsVerified, resourceController.updateResource);

router
  .route('/:resource/:id')
  .get(auth('readResource'), userEmailIsVerified, resourceController.getResource)
  .put(auth('writeResource'), userEmailIsVerified, resourceController.updateResource)
  .delete(auth('writeResource'), userEmailIsVerified, resourceController.deleteResource);

module.exports = router;
