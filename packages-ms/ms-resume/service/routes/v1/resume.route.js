const express = require('express');
const { ResumeValidator } = require('@resumapi/lib-openapi');
const auth = require('../../middlewares/auth');
const userEmailIsVerified = require('../../middlewares/userEmailIsVerified');
const { resumeController } = require('../../controllers');

const router = express.Router();

router.use(ResumeValidator);

router
  .route('/')
  .get(auth('getResume'), userEmailIsVerified, resumeController.getResume)
  .post(auth('manageResume'), userEmailIsVerified, resumeController.createResume)
  .put(auth('manageResume'), userEmailIsVerified, resumeController.updateResume)
  .delete(auth('manageResume'), userEmailIsVerified, resumeController.deleteResume);

// Public resume route
router.route('/:user').get(resumeController.getResumeForUser);

module.exports = router;
