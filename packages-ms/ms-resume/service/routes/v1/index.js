const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const resumeRoute = require('./resume.route');
const resourceRoute = require('./resource.route');
const healthRoute = require('./health.route');

module.exports = (app) => {
  const router = express.Router();

  const defaultRoutes = [
    {
      path: '/health',
      route: healthRoute,
    },
    {
      path: '/auth',
      route: authRoute,
    },
    {
      path: '/users',
      route: userRoute,
    },
    {
      path: '/resume',
      route: resumeRoute,
    },
    {
      path: '/resource',
      route: resourceRoute,
    },
  ];

  defaultRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });

  app.use('/v1/', router);
};
