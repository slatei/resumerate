const { UnauthorizedError } = require('@resumapi/lib-error');

const userEmailIsVerified = (req, res, next) => {
  const { user } = req;
  if (!user.isEmailVerified) {
    return next(new UnauthorizedError('User must have a verified email to continue'));
  }

  return next();
};

module.exports = userEmailIsVerified;
