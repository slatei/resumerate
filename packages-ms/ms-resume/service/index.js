const express = require('express');
const helmet = require('helmet');
const xss = require('xss-clean');
const mongoSanitize = require('express-mongo-sanitize');
const compression = require('compression');
const cors = require('cors');
const passport = require('passport');
const logger = require('@resumapi/lib-logger');
const database = require('@resumapi/lib-database');
const { errorHandler, NotFoundError } = require('@resumapi/lib-error');
const { jwtStrategy } = require('./config/passport');
const defineRoutes = require('./routes/v1');

function createApp() {
  const app = express();

  database.connect();

  // set security HTTP headers
  app.use(helmet());

  // Configure body parsers
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  // sanitize request data
  app.use(xss());
  app.use(mongoSanitize());

  // gzip compression
  app.use(compression());

  // enable cors
  app.use(cors());
  app.options('*', cors());

  // jwt authentication
  app.use(passport.initialize());
  passport.use('jwt', jwtStrategy);

  app.enable('trust proxy');

  // Log request url and status code
  app.use((req, res, next) => {
    res.on('finish', () => {
      logger.info(`${req.url} ${res.statusCode}`);
    });
    next();
  });

  defineRoutes(app);

  // send back a 404 error for any unknown api request
  app.use((req, res, next) => {
    next(new NotFoundError('Route not recognised', req.path));
  });

  app.use(errorHandler.handleErrorMiddleware);

  return app;
}
exports.createApp = createApp;
