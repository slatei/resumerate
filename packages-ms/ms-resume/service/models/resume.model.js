const { Schema, model } = require('mongoose');
const { toJSON } = require('./plugins');
const {
  summarySchema,
  workSchema,
  volunteerSchema,
  educationSchema,
  awardSchema,
  publicationSchema,
  skillSchema,
  languageSchema,
  interestSchema,
  referenceSchema,
  projectSchema,
  certificateSchema,
} = require('./schemas');

const resumeSchema = Schema({
  id: { type: Schema.Types.ObjectId, private: true },
  basics: summarySchema,
  work: [workSchema],
  volunteer: [volunteerSchema],
  education: [educationSchema],
  awards: [awardSchema],
  certificates: [certificateSchema],
  publications: [publicationSchema],
  skills: [skillSchema],
  languages: [languageSchema],
  interests: [interestSchema],
  references: [referenceSchema],
  projects: [projectSchema],
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
    private: true,
  },
});

// Add plugin that converts mongoose to json
resumeSchema.plugin(toJSON);

/**
 * @typedef Resume
 */
const Resume = model('Resume', resumeSchema);

module.exports = Resume;
