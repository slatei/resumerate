const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('../plugins');

const certificateSchema = Schema({
  id: Schema.Types.ObjectId,
  name: { type: String },
  date: {
    type: String,
    validate(value) {
      if (!validator.isISO8601(value)) {
        throw new Error('Invalid Date');
      }
    },
  },
  url: {
    type: String,
    validate(value) {
      if (!validator.isURL(value)) {
        throw new Error('Invalid URL');
      }
    },
  },
  issuer: { type: String },
});

certificateSchema.plugin(toJSON);
certificateSchema.plugin(paginate);
module.exports.certificateSchema = certificateSchema;

// /**
//  * @typedef Certificate
//  */
// const Certificate = model('Certificate', certificateSchema);
// module.exports.Certificate = Certificate;
