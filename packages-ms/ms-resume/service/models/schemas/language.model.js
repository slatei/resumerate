const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const { toJSON, paginate } = require('../plugins');

const languageSchema = Schema({
  id: Schema.Types.ObjectId,
  language: { type: String },
  fluency: {
    type: String,
    enum: [
      'No proficiency',
      'Elementary',
      'Limited working',
      'Professional working',
      'Full professional',
      'Native or bilingual',
    ],
  },
});

languageSchema.plugin(toJSON);
languageSchema.plugin(paginate);
module.exports.languageSchema = languageSchema;

// /**
//  * @typedef Language
//  */
// const Language = model('Language', languageSchema);
// module.exports.Language = Language;
