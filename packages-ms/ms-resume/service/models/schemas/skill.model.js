const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const { toJSON, paginate } = require('../plugins');

const skillSchema = Schema({
  id: Schema.Types.ObjectId,
  name: { type: String },
  level: { type: String },
  keywords: [{ type: String }],
});

skillSchema.plugin(toJSON);
skillSchema.plugin(paginate);
module.exports.skillSchema = skillSchema;

// /**
//  * @typedef Skill
//  */
// const Skill = model('Skill', skillSchema);
// module.exports.Skill = Skill;
