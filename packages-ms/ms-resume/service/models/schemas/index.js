const { awardSchema } = require('./award.model');
const { certificateSchema } = require('./certificate.model');
const { educationSchema } = require('./education.model');
const { interestSchema } = require('./interest.model');
const { languageSchema } = require('./language.model');
const { projectSchema } = require('./project.model');
const { publicationSchema } = require('./publication.model');
const { referenceSchema } = require('./reference.model');
const { skillSchema } = require('./skill.model');
const { summarySchema } = require('./summary.model');
const { volunteerSchema } = require('./volunteer.model');
const { workSchema } = require('./work.model');

// Schema Exports
module.exports = {
  awardSchema,
  certificateSchema,
  educationSchema,
  interestSchema,
  languageSchema,
  projectSchema,
  publicationSchema,
  referenceSchema,
  skillSchema,
  summarySchema,
  volunteerSchema,
  workSchema,
};
