const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('../plugins');

const awardSchema = Schema({
  id: Schema.Types.ObjectId,
  title: { type: String },
  date: {
    type: String,
    validate(value) {
      if (!validator.isISO8601(value)) {
        throw new Error('Invalid Date');
      }
    },
  },
  awarder: { type: String },
  summary: { type: String },
});

awardSchema.plugin(toJSON);
awardSchema.plugin(paginate);
module.exports.awardSchema = awardSchema;

// /**
//  * @typedef Award
//  */
// const Award = model('Award', awardSchema);
// module.exports.Award = Award;
