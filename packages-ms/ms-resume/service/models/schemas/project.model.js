const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('../plugins');

const projectSchema = Schema({
  id: Schema.Types.ObjectId,
  name: { type: String },
  description: { type: String },
  highlights: [{ type: String }],
  keywords: [{ type: String }],
  startDate: {
    type: String,
    validate(value) {
      if (!validator.isISO8601(value)) {
        throw new Error('Invalid Date');
      }
    },
  },
  endDate: {
    type: String,
    validate(value) {
      if (!validator.isISO8601(value)) {
        throw new Error('Invalid Date');
      }
    },
  },
  url: {
    type: String,
    validate(value) {
      if (!validator.isURL(value)) {
        throw new Error('Invalid URL');
      }
    },
  },
  roles: [{ type: String }],
  entity: { type: String },
  type: { type: String },
});

projectSchema.plugin(toJSON);
projectSchema.plugin(paginate);
module.exports.projectSchema = projectSchema;

// /**
//  * @typedef Project
//  */
// const Project = model('Project', projectSchema);
// module.exports.Project = Project;
