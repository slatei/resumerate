const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('../plugins');

const workSchema = Schema({
  id: Schema.Types.ObjectId,
  name: { type: String },
  location: { type: String },
  description: { type: String },
  position: { type: String },
  url: {
    type: String,
    validate(value) {
      if (!validator.isURL(value)) {
        throw new Error('Invalid URL');
      }
    },
  },
  startDate: {
    type: String,
    validate(value) {
      if (!validator.isISO8601(value)) {
        throw new Error('Invalid Date');
      }
    },
  },
  endDate: {
    type: String,
    validate(value) {
      if (!validator.isISO8601(value)) {
        throw new Error('Invalid Date');
      }
    },
  },
  summary: String,
  highlights: [{ type: String }],
});

workSchema.plugin(toJSON);
workSchema.plugin(paginate);
module.exports.workSchema = workSchema;

// /**
//  * @typedef Work
//  */
// const Work = model('Work', workSchema);
// module.exports.Work = Work;
