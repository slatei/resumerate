const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('../plugins');

const volunteerSchema = Schema({
  id: Schema.Types.ObjectId,
  organization: { type: String },
  position: { type: String, required: true },
  url: {
    type: String,
    validate(value) {
      if (!validator.isURL(value)) {
        throw new Error('Invalid URL');
      }
    },
  },
  startDate: {
    type: String,
    validate(value) {
      if (!validator.isISO8601(value)) {
        throw new Error('Invalid Date');
      }
    },
  },
  endDate: {
    type: String,
    validate(value) {
      if (!validator.isISO8601(value)) {
        throw new Error('Invalid Date');
      }
    },
  },
  summary: { type: String },
  highlights: [{ type: String }],
});

volunteerSchema.plugin(toJSON);
volunteerSchema.plugin(paginate);
module.exports.volunteerSchema = volunteerSchema;

// /**
//  * @typedef Volunteer
//  */
// const Volunteer = model('Volunteer', volunteerSchema);
// module.exports.Volunteer = Volunteer;
