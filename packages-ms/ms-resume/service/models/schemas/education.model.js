const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('../plugins');

const educationSchema = Schema({
  id: Schema.Types.ObjectId,
  institution: { type: String },
  url: {
    type: String,
    validate(value) {
      if (!validator.isURL(value)) {
        throw new Error('Invalid URL');
      }
    },
  },
  area: { type: String },
  studyType: { type: String },
  startDate: {
    type: String,
    validate(value) {
      if (!validator.isISO8601(value)) {
        throw new Error('Invalid Date');
      }
    },
  },
  endDate: {
    type: String,
    validate(value) {
      if (!validator.isISO8601(value)) {
        throw new Error('Invalid Date');
      }
    },
  },
  score: { type: String },
  courses: [{ type: String }],
});

educationSchema.plugin(toJSON);
educationSchema.plugin(paginate);
module.exports.educationSchema = educationSchema;

// /**
//  * @typedef Education
//  */
// const Education = model('Education', educationSchema);
// module.exports.Education = Education;
