const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const validator = require('validator');
const { toJSON } = require('../plugins');

const profileSchema = Schema({
  _id: false,
  network: { type: String },
  username: { type: String },
  url: {
    type: String,
    validate(value) {
      if (!validator.isURL(value)) {
        throw new Error('Invalid URL');
      }
    },
  },
});

const summarySchema = Schema({
  _id: false,
  name: { type: String, required: true },
  label: { type: String, required: true },
  image: { type: String },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error('Invalid email');
      }
    },
  },
  phone: { type: String },
  url: {
    type: String,
    validate(value) {
      if (!validator.isURL(value)) {
        throw new Error('Invalid URL');
      }
    },
  },
  summary: { type: String },
  location: {
    address: { type: String },
    postalCode: { type: String },
    city: { type: String },
    countryCode: { type: String },
    region: { type: String },
  },
  profiles: [profileSchema],
});

summarySchema.plugin(toJSON);
module.exports.summarySchema = summarySchema;

// /**
//  * @typedef Summary
//  */
// const Summary = model('Summary', summarySchema);
// module.exports.Summary = Summary;
