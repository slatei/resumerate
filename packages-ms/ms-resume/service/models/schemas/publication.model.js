const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const validator = require('validator');
const { toJSON, paginate } = require('../plugins');

const publicationSchema = Schema({
  id: Schema.Types.ObjectId,
  name: { type: String },
  publisher: { type: String },
  releaseDate: {
    type: String,
    validate(value) {
      if (!validator.isISO8601(value)) {
        throw new Error('Invalid Date');
      }
    },
  },
  url: {
    type: String,
    validate(value) {
      if (!validator.isURL(value)) {
        throw new Error('Invalid URL');
      }
    },
  },
  summary: { type: String },
});

publicationSchema.plugin(toJSON);
publicationSchema.plugin(paginate);
module.exports.publicationSchema = publicationSchema;

// /**
//  * @typedef Publication
//  */
// const Publication = model('Publication', publicationSchema);
// module.exports.Publication = Publication;
