const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const { toJSON, paginate } = require('../plugins');

const referenceSchema = Schema({
  id: Schema.Types.ObjectId,
  name: { type: String },
  reference: { type: String },
});

referenceSchema.plugin(toJSON);
referenceSchema.plugin(paginate);
module.exports.referenceSchema = referenceSchema;

// /**
//  * @typedef Reference
//  */
// const Reference = model('Reference', referenceSchema);
// module.exports.Reference = Reference;
