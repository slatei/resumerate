const { Schema } = require('mongoose');
// const { model } = require('mongoose');
const { toJSON, paginate } = require('../plugins');

const interestSchema = Schema({
  id: Schema.Types.ObjectId,
  name: { type: String },
  keywords: [{ type: String }],
});

interestSchema.plugin(toJSON);
interestSchema.plugin(paginate);
module.exports.interestSchema = interestSchema;

// /**
//  * @typedef Interest
//  */
// const Interest = model('Interest', interestSchema);
// module.exports.Interest = Interest;
