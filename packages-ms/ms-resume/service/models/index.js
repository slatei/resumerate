// const { Award } = require('./schemas/award.model');
// const { Certificate } = require('./schemas/certificate.model');
// const { Education } = require('./schemas/education.model');
// const { Interest } = require('./schemas/interest.model');
// const { Language } = require('./schemas/language.model');
// const { Project } = require('./schemas/project.model');
// const { Publication } = require('./schemas/publication.model');
// const { Reference } = require('./schemas/reference.model');
// const { Skill } = require('./schemas/skill.model');
// const { Summary } = require('./schemas/summary.model');
// const { Volunteer } = require('./schemas/volunteer.model');
// const { Work } = require('./schemas/work.model');

// Model exports
// module.exports.Award = Award;
// module.exports.Certificate = Certificate;
// module.exports.Education = Education;
// module.exports.Interest = Interest;
// module.exports.Language = Language;
// module.exports.Project = Project;
// module.exports.Publication = Publication;
// module.exports.Reference = Reference;
// module.exports.Skill = Skill;
// module.exports.Summary = Summary;
// module.exports.Volunteer = Volunteer;
// module.exports.Work = Work;

module.exports.Token = require('./token.model');
module.exports.User = require('./user.model');
module.exports.Resume = require('./resume.model');
