const { createApp } = require('./service');

// Exports the express app
module.exports = createApp();
