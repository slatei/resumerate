const express = require('express');
const logger = require('@resumapi/lib-logger');
const { Record } = require('./models');
const util = require('util');
const { TestValidator } = require('@resumapi/lib-openapi');

module.exports = (app) => {
  const router = express.Router();

  router.get('/', async (req, res, next) => {
    try {
      res.json({ name: 'world', level: 2 });
    } catch (error) {
      next(error);
    }
  });

  router.post('/', async (req, res, next) => {
    try {
      logger.info(`Creating new test record ${util.inspect(req.body)}`);

      const record = await Record.create(req.body);

      return res.status(201).json(record);
    } catch (error) {
      next(error);
      return undefined;
    }
  });

  app.use(TestValidator);
  app.use('/', router);
};
