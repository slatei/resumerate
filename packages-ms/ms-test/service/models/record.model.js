const { Schema, model } = require('mongoose');

const recordSchema = Schema({
  id: Schema.Types.ObjectId,
  name: { type: String },
  level: { type: Number },
  keywords: [{ type: String }],
});

/**
 * @typedef Record
 */
const Record = model('Record', recordSchema);

module.exports = { recordSchema, Record };
