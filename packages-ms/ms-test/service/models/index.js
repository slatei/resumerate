const { Record, recordSchema } = require('./record.model');

module.exports = {
  Record,
  recordSchema,
};
