const express = require('express');
const helmet = require('helmet');
const logger = require('@resumapi/lib-logger');
const defineRoutes = require('./routes');
const cors = require('cors');
const database = require('@resumapi/lib-database');
const { errorHandler } = require('@resumapi/lib-error');

function createApp() {
  const app = express();

  database.connect();

  app.use(helmet());

  // Configure body parsers
  app.use(express.json());
  app.use(express.text());
  app.use(express.urlencoded({ extended: false }));

  app.use(cors());
  app.options('*', cors());

  defineRoutes(app);

  app.use(errorHandler.handleErrorMiddleware);

  return app;
}
exports.createApp = createApp;
