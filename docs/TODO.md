# ResumAPI Todo List

## Priority

- [ ] Authentication and jwt validation
- [ ] Update database interface to check connectivity, and don't allow server to start if no connection is available
      (This is currently happening when running docker, or when database container is unavailable)

## Deployment

- [x] Use the ms-test to test launching the practica-js service framework in GCP cloud functions
- [ ] Update CI deployment

## Libraries

- [x] Create database library
- [ ] Create database adaptor
- [ ] Separate out passport/JWT auth library
- [ ] Create constants lib
- [x] Add validation middleware to lib-openapi
- [x] Allow .env injection of variables over convict
- [x] Standardise error handling and error objects

## Resumapi Development

- [ ] Mock the email API - Need to do this sooner rather than later to remove the .env requirement of ethereal auth
- [ ] Migrate openapi to latest version
- [ ] Resolve failing tests
- [ ] Package level unit testing
- [ ] Figure out way of setting helmet and security policies in lib-broker

## UI Development

- [ ] Create separate swagger UI web app

## Repository Maintenance

- [x] Add standard versioning
- [ ] Project level documentation
- [ ] Package level documentation
- [ ] Move major audit and lint precommit hooks to be prerelease hooks
- [x] Add markdownlint cli and config <https://github.com/DavidAnson/markdownlint>
- [ ] Wipe git history with BFG tool <https://rtyley.github.io/bfg-repo-cleaner/>
- [ ] Use standard-version for openapi versioning
  - <https://github.com/conventional-changelog/standard-version#can-i-use-standard-version-for-additional-metadata-files-languages-or-version-files>
  - Enter prompt into chatgpt: "Use npm "standard-version" library to update an openapi version according to conventional commit messages"
- [ ] Consider replacing standard-version with lerna
- [ ] Integrate JSDoc?
- [ ] Move lint-staged and syncpack to their own libraries
