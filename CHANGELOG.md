# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.3.0](https://gitlab.com/slatei/resumerate/compare/v0.2.1...v0.3.0) (2023-09-02)

### ⚠ BREAKING CHANGES

- **resume:** Resume API functionally complete, endpoints working correctly

### Features

- Add mongodb database library, and demonstrate with ms-test ([cc28bef](https://gitlab.com/slatei/resumerate/commit/cc28bef4e7aa3ccee9f8d1db1c0ab0a6a7451a2d))
- Implement common error handling library ([30aa815](https://gitlab.com/slatei/resumerate/commit/30aa815741d55b9939265a0fae5c158103493ffc))
- **resume:** Resume API functionally complete, endpoints working correctly ([57cf90f](https://gitlab.com/slatei/resumerate/commit/57cf90fc8e7d04ed6049e976063f7bb4eeee9797))
- Upgraded error handling and error definitions ([f9f87df](https://gitlab.com/slatei/resumerate/commit/f9f87dff0c529da749b93648e0499d5b2ac5f3d1))

### [0.2.1](https://gitlab.com/slatei/resumerate/compare/v0.2.0...v0.2.1) (2023-08-25)

### Features

- Add broker library for instantiating http/s app server ([0a92a50](https://gitlab.com/slatei/resumerate/commit/0a92a50bcf28434c1453e769c9e517b086796827))
- Add test api service for deploy testing ([3e8b651](https://gitlab.com/slatei/resumerate/commit/3e8b6519039a0a52e251e6ed041a0822a926d24d))
- Create docker deployment tooling, and example api ([4bee79c](https://gitlab.com/slatei/resumerate/commit/4bee79ca3e431219a399b88283963502af408162))
- Implement google cloud functions deployment method ([db1781f](https://gitlab.com/slatei/resumerate/commit/db1781f281880d8d8d7b23037a9996de67e56006))
- Migrate service configuration to shared config library ([fd964f8](https://gitlab.com/slatei/resumerate/commit/fd964f8c62c32f08641b3dbebd6074dbc206ebdb))

### Bug Fixes

- Run audit scans and remove compromised packages ([82abb65](https://gitlab.com/slatei/resumerate/commit/82abb65218edb5d937e836079137fb3ad77deba7))

## [0.2.0](https://gitlab.com/slatei/resumerate/compare/v0.1.0...v0.2.0) (2023-08-14)

### Development

- Fix issues with invalid library package versions ([041c3f1](https://gitlab.com/slatei/resumerate/commit/041c3f17571decfea31e35ccdc5cdafd5d19a683))

### Documentation

- Add links and references to configuration files ([9974dcb](https://gitlab.com/slatei/resumerate/commit/9974dcbd08c8e24efa010317f882e4e8a3173783))
- Add todo item for shfmt ([02c5319](https://gitlab.com/slatei/resumerate/commit/02c5319e8858b895935d1944950ce6ee9fd52f89))

### Refactoring

- Add husky and commitlint to project repo ([0965d9e](https://gitlab.com/slatei/resumerate/commit/0965d9e87e4a0760df20287b58a5d996fcd742f5))
- Add lint staged for openapi spectral lint and postman formatter ([7634149](https://gitlab.com/slatei/resumerate/commit/76341493043d998937f7ad848b998bced40da5aa))
- Add lint-staged to lint and format staged files ([6aff371](https://gitlab.com/slatei/resumerate/commit/6aff3712c3765627a1cb86cf8d0a7e3fa2f27985))
- Add pre-commit linting ([c1d4f2a](https://gitlab.com/slatei/resumerate/commit/c1d4f2a17f82b3cdd1497d84c1d5cec251bc37a1))
- Add pre-commit linting ([40ba1a8](https://gitlab.com/slatei/resumerate/commit/40ba1a8e8270e88fe83ab625974c74e793a6ff63))
- Add project prettier-fix logic ([ba6198c](https://gitlab.com/slatei/resumerate/commit/ba6198c3725913c5f80ab831ada79b3196259462))
- Add standard-version library for project maintenance ([74fb6bc](https://gitlab.com/slatei/resumerate/commit/74fb6bcf8cfdf0ce4f8d7a5ead5f005e62e52db9))
- Add syncpack for managing dependency versions and formatting ([5ae5202](https://gitlab.com/slatei/resumerate/commit/5ae520284bde4d249cc87ae202dc5ae1df3a3a83))
- Configure lint-staged to work with syncpack format ([7399d5a](https://gitlab.com/slatei/resumerate/commit/7399d5acf16a8a9a1ac5515d2f926820f2754ac4))
- Fix spelling mistakes and update dictionaries ([9b54f60](https://gitlab.com/slatei/resumerate/commit/9b54f6055a633d35df81868caf78a4ecc6cd775c))
- Migrate configurations to a reusable config library using convict ([81e2fed](https://gitlab.com/slatei/resumerate/commit/81e2fed4f62bbb0cf41ed0d8e3d81def0d9eebd1))
- Move common logging library to package ([7a6d5fa](https://gitlab.com/slatei/resumerate/commit/7a6d5fa3ff265d08c973b03005bc8cfe6f06ac2f))
- Move prettier config to its own package ([a410cae](https://gitlab.com/slatei/resumerate/commit/a410cae8829a8195283bfcd555a15527546dd821))
- Move prettierconfig into root package.json ([3f5c0ab](https://gitlab.com/slatei/resumerate/commit/3f5c0ab88f7529ff4e5edf3488fefb76ded8c8c5))
- Resolve repo wide lint:fix commands and fix linting for test-postman ([87e7c66](https://gitlab.com/slatei/resumerate/commit/87e7c6601b68e91a1b539ce2923c08f279834d69))
- Run quality fix for project and fix package.json files ([11ac9db](https://gitlab.com/slatei/resumerate/commit/11ac9dbe1ab368db030c4d664a82243707f9253d))
- Update syncpack to use opinionated package.json formatter ([ca2ccfd](https://gitlab.com/slatei/resumerate/commit/ca2ccfddebf2f905a966faf510c008bc173d482a))
