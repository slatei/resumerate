import Welcome from '../../ui-components/Welcome';

export default {
  title: 'Example/Welcome',
  component: Welcome,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
  tags: ['autodocs'],
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'fullscreen',
  },
};

// // The storiesOf API is used to display stories in storybook
// import { storiesOf } from '@storybook/react';
// // Importing our react component
// import Welcome from '../../ui-components/Welcome';
// // Displaying the component
// storiesOf('Welcome', module).add('Welcome component', () => <Welcome name="Nic"></Welcome>);
