import config from '@lib/config';
import React, { useState, useEffect } from 'react';

function useResume() {
  const [resume, setResume] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(`${config.apiUrl}/resume/${config.userId}`)
      .then((response) => {
        if (response.status !== 200) {
          throw new Error(`Bad response from server: ${response.status}`);
        }
        return response.json();
      })
      .then((response) => {
        setLoading(false);
        setResume(response);
        setError('');
      })
      .catch((error) => {
        setLoading(false);
        setResume(null);
        setError(String(error));
      });
  }, []);

  return [resume, loading, error];
}

export default useResume;
