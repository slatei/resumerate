const shortDate = (date) => {
  return date.substr(0, 10);
};

module.exports.shortDate = shortDate;
