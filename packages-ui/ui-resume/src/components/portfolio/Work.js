import { ResumeContext } from '@components/Portfolio';
import React, { useContext } from 'react';
import { shortDate } from '@lib/utils';

function Work() {
  const { work: jobs } = useContext(ResumeContext);

  return (
    <div>
      {jobs ? (
        <section id="work" className="row">
          <aside className="col-sm-3">
            <h3>Work</h3>
          </aside>

          <div className="col-sm-9">
            <div className="row">
              {jobs.map((work) => {
                return (
                  <div className="col-sm-12">
                    <h4 className="strike-through">
                      <span>{work.company}</span>
                      <span className="date">
                        {shortDate(work?.startDate)} — {shortDate(work?.endDate)}
                      </span>
                    </h4>

                    <div className="website pull-right">
                      <a href={work.url}>{work.url}</a>
                    </div>

                    <div className="position">{work.position}</div>

                    <div className="summary">
                      <p>{work.summary}</p>
                    </div>

                    {work.highlights ? (
                      <>
                        <h4>Highlights</h4>
                        <ul className="highlights">
                          {work.highlights.map((highlight) => {
                            return <li className="bullet">{highlight}</li>;
                          })}
                        </ul>
                      </>
                    ) : null}
                  </div>
                );
              })}
            </div>
          </div>
        </section>
      ) : null}
    </div>
  );
}

export default Work;
