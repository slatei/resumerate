import { ResumeContext } from '@components/Portfolio';
import React, { useContext } from 'react';

function Skills() {
  const { skills } = useContext(ResumeContext);

  return (
    <div>
      {skills ? (
        <section id="skills" className="row">
          <aside className="col-sm-3">
            <h3>Skills</h3>
          </aside>

          <div className="col-sm-9">
            <div className="row">
              {skills.map((skill) => {
                return (
                  <div className="col-sm-6">
                    <div className="name">
                      <h4>{skill.name}</h4>
                    </div>

                    {skill.keywords ? (
                      <>
                        <ul className="keywords">
                          {skill.keywords.map((keyword) => {
                            return <li>{keyword}</li>;
                          })}
                        </ul>
                      </>
                    ) : null}
                  </div>
                );
              })}
            </div>
          </div>
        </section>
      ) : null}
    </div>
  );
}

export default Skills;
