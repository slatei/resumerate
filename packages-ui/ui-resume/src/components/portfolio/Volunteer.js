import { ResumeContext } from '@components/Portfolio';
import React, { useContext } from 'react';
import { shortDate } from '@lib/utils';

function Volunteer() {
  const { volunteer: volunteerList } = useContext(ResumeContext);

  return (
    <div>
      {volunteerList ? (
        <section id="volunteer" className="row">
          <aside className="col-sm-3">
            <h3>Volunteer</h3>
          </aside>

          <div className="col-sm-9">
            <div className="row">
              {volunteerList.map((volunteer) => {
                return (
                  <div className="col-sm-12">
                    <h4 className="strike-through">
                      <span>{volunteer.organization}</span>
                      <span className="date">
                        {shortDate(volunteer.startDate)} — {shortDate(volunteer.endDate)}
                      </span>
                    </h4>

                    <div className="website pull-right">
                      <a href={volunteer.url}>{volunteer.url}</a>
                    </div>

                    <div className="position">{volunteer.position}</div>

                    <div className="summary">
                      <p>{volunteer.summary}</p>
                    </div>

                    {volunteer.highlights ? (
                      <>
                        <h4>Highlights</h4>
                        <ul className="highlights">
                          {volunteer.highlights.map((highlight) => {
                            return <li className="bullet">{highlight}</li>;
                          })}
                        </ul>
                      </>
                    ) : null}
                  </div>
                );
              })}
            </div>
          </div>
        </section>
      ) : null}
    </div>
  );
}

export default Volunteer;
