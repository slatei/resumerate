import { ResumeContext } from '@components/Portfolio';
import React, { useContext } from 'react';

function Profiles() {
  const { basics } = useContext(ResumeContext);
  const { profiles } = basics;

  if (profiles.length) {
    return (
      <div>
        {profiles ? (
          <section id="profiles" className="row">
            <aside className="col-sm-3">
              <h3>Profiles</h3>
            </aside>

            <div className="col-sm-9">
              <div className="row">
                {profiles.map((profile) => {
                  return (
                    <div className="col-sm-6">
                      <strong className="network">{profile.network}</strong>
                      <div className="username">
                        <div className="url">
                          <a href={profile.url}>{profile.username}</a>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </section>
        ) : null}
      </div>
    );
  } else return null;
}

export default Profiles;
