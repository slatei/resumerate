import { ResumeContext } from '@components/Portfolio';
import React, { useContext } from 'react';

function References() {
  const { references } = useContext(ResumeContext);

  return (
    <div>
      {references ? (
        <section id="references" class="row">
          <aside class="col-sm-3">
            <h3>References</h3>
          </aside>

          <div class="col-sm-9">
            <div class="row">
              {references.map((reference) => {
                return (
                  <div class="col-sm-12">
                    <blockquote class="reference">
                      <p>{reference.reference}</p>

                      <p class="name">
                        <strong>— {reference.name}</strong>
                      </p>
                    </blockquote>
                  </div>
                );
              })}
            </div>
          </div>
        </section>
      ) : null}
    </div>
  );
}

export default References;
