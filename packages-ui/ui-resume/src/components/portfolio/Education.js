import { ResumeContext } from '@components/Portfolio';
import React, { useContext } from 'react';
import { shortDate } from '@lib/utils';

function Education() {
  const { education: educationList } = useContext(ResumeContext);

  return (
    <div>
      {educationList ? (
        <section id="education" className="row">
          <aside className="col-sm-3">
            <h3>Education</h3>
          </aside>

          <div className="col-sm-9">
            <div className="row">
              {educationList.map((education) => {
                return (
                  <div className="col-sm-12">
                    <h4 className="strike-through">
                      <span>{education.institution}</span>
                      <span className="date">
                        {shortDate(education.startDate)} — {shortDate(education.endDate)}
                      </span>
                    </h4>

                    <div className="area">{education.area}</div>

                    <div className="studyType">{education.studyType}</div>

                    {education.courses ? (
                      <>
                        <h4>Courses</h4>
                        <ul className="courses">
                          {education.courses.map((course) => {
                            return <li>{course}</li>;
                          })}
                        </ul>
                      </>
                    ) : null}
                  </div>
                );
              })}
            </div>
          </div>
        </section>
      ) : null}
    </div>
  );
}

export default Education;
