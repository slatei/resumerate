import { ResumeContext } from '@components/Portfolio';
import React, { useContext } from 'react';

function Languages() {
  const { languages } = useContext(ResumeContext);

  return (
    <div>
      {languages ? (
        <section id="languages" className="row">
          <aside className="col-sm-3">
            <h3>Languages</h3>
          </aside>

          <div className="col-sm-9">
            <div className="row">
              {languages.map((language) => {
                return (
                  <div className="col-sm-6">
                    <div className="language">
                      <strong>{language.language}</strong>
                    </div>

                    <div className="fluency">{language.fluency}</div>
                  </div>
                );
              })}
            </div>
          </div>
        </section>
      ) : null}
    </div>
  );
}

export default Languages;
