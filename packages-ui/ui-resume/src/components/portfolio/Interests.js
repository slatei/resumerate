import { ResumeContext } from '@components/Portfolio';
import React, { useContext } from 'react';

function Interests() {
  const { interests } = useContext(ResumeContext);

  return (
    <div>
      {interests ? (
        <section id="interests" className="row">
          <aside className="col-sm-3">
            <h3>Interests</h3>
          </aside>

          <div className="col-sm-9">
            <div className="row">
              {interests.map((interest) => {
                return (
                  <div className="col-sm-6">
                    <div className="name">
                      <h4>{interest.name}</h4>
                    </div>

                    {interest.keywords ? (
                      <>
                        <ul className="keywords">
                          {interest.keywords.map((keyword) => {
                            return <li>{keyword}</li>;
                          })}
                        </ul>
                      </>
                    ) : null}
                  </div>
                );
              })}
            </div>
          </div>
        </section>
      ) : null}
    </div>
  );
}

export default Interests;
