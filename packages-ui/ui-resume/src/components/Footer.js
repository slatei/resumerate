import styles from '@styles/Footer.module.css';

export default function Footer() {
  return (
    <>
      <footer className={styles.footer}>Thorpincorp Ltd. 2021</footer>
    </>
  );
}
