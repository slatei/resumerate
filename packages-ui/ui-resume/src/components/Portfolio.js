import React from 'react';
import Awards from '@components/portfolio/Awards';
import Education from '@components/portfolio/Education';
import Interests from '@components/portfolio/Interests';
import Languages from '@components/portfolio/Languages';
import Profiles from '@components/portfolio/Profiles';
import Publications from '@components/portfolio/Publications';
import References from '@components/portfolio/References';
import Skills from '@components/portfolio/Skills';
import Summary from '@components/portfolio/Summary';
import Volunteer from '@components/portfolio/Volunteer';
import Work from '@components/portfolio/Work';
import useResume from '@lib/useResume';
import Header from '@components/Header';

export const ResumeContext = React.createContext();

function Portfolio() {
  const [resume, isResumeLoading, isResumeError] = useResume();

  if (isResumeLoading) {
    return <div>Loading</div>;
  }

  if (!resume) {
    return <div>Not found</div>;
  }

  return (
    <div>
      <ResumeContext.Provider value={resume}>
        <Header />

        <div id="content" class="container">
          <Summary />
          <Work />
          <Volunteer />
          <Education />
          <Awards />
          <Publications />
          <Skills />
          <Languages />
          <Interests />
          <References />
        </div>
      </ResumeContext.Provider>
    </div>
  );
}

export default Portfolio;
