import Head from 'next/head';
import Footer from '@components/Footer';
import Portfolio from '@components/Portfolio';

export default function Home() {
  return (
    <div>
      <Head>
        <title>Thorpincorp Resume</title>
        <link rel="icon" href="/favicon.ico" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, user-scalable=no, minimal-ui"></meta>

        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/css/bootstrap.min.css"
        ></link>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/2.0.2/octicons.min.css"></link>
      </Head>

      <main>
        <Portfolio />
      </main>

      <Footer />
    </div>
  );
}
