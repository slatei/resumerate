const isProd = process.env.NODE_ENV === 'production';

module.exports = {
  output: 'export',
  // Use the CDN in production and localhost for development.
  assetPrefix: isProd ? 'https://storage.googleapis.com/portfolio.thorpincorp.com' : 'http://localhost:3000',
  env: {
    RESUME_API_URL: isProd
      ? 'https://us-central1-thorpincorp-310208.cloudfunctions.net/resume/v1'
      : 'https://us-central1-thorpincorp-310208.cloudfunctions.net/resume/v1',
  },
};
