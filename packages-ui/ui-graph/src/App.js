import React, { useState, useEffect, useRef } from 'react';
import { Box, CircularProgress } from '@mui/material';
import './App.css';
import Graph from 'components/Graph';
import Header from 'components/Header';
import InfoOverlay from 'components/InfoOverlay';
import NotFound from 'components/NotFound';
import useResume from 'lib/useResume';
import useWindow from 'lib/useWindow';

export const ResumeContext = React.createContext();

const App = (props) => {
  const [resume, graphData, isResumeLoading, isResumeError] = useResume();
  const [windowState] = useWindow();
  const [activeGraphData, setActiveGraphData] = useState({
    data: {
      nodes: [],
      links: [],
    },
    selectedNode: null,
  });

  const forceGraphRef = useRef();

  // Initialise the active graph
  useEffect(() => {
    // const fg = forceGraphRef.current;

    // // Deactivate existing forces
    // fg.d3Force('center', null);
    // fg.d3Force('charge', null);

    // // Add collision and bounding box forces
    // fg.d3Force('collide', d3.forceCollide(4));
    // fg.d3Force('box', () => {
    //   const SQUARE_HALF_SIDE = N * 2;

    //   nodes.forEach((node) => {
    //     const x = node.x || 0,
    //       y = node.y || 0;

    //     // bounce on box walls
    //     if (Math.abs(x) > SQUARE_HALF_SIDE) {
    //       node.vx *= -1;
    //     }
    //     if (Math.abs(y) > SQUARE_HALF_SIDE) {
    //       node.vy *= -1;
    //     }
    //   });
    // });

    setActiveGraphData((state) => ({
      ...state,
      data: { nodes: graphData.nodes, links: graphData.links },
    }));
  }, [graphData]);

  if (isResumeLoading) {
    return (
      <Box sx={{ display: 'flex', justifyContent: 'center', marginTop: 10 }}>
        <CircularProgress />
      </Box>
    );
  }

  if (isResumeError || !resume || !graphData) {
    return <NotFound message={isResumeError} />;
  }

  const handleNodeClick = (selectedNode) => {
    const links = graphData.links.filter((link) => {
      return link.source.id === selectedNode.id || link.target.id === selectedNode.id;
    });
    const nodes = graphData.nodes.filter((node) => {
      return (
        node.id === selectedNode.id ||
        links.reduce((isIn, curr) => {
          return isIn || node.id === curr.source.id || node.id === curr.target.id;
        }, false)
      );
    });

    updateNodesAndLinks(selectedNode, nodes, links);
    forceGraphRef.current?.centerAt(selectedNode.x, selectedNode.y, 1000);
    forceGraphRef.current?.zoom(3, 1000);
  };

  const handleHomeButtonClick = () => {
    updateNodesAndLinks(null, graphData.nodes, graphData.links);
    forceGraphRef.current?.centerAt(0, 0, 1000);
    forceGraphRef.current?.zoom(1, 1000);
  };

  const handleBackgroundClick = () => {
    updateNodesAndLinks(null, graphData.nodes, graphData.links);
  };

  const updateNodesAndLinks = (selectedNode, nodes, links) => {
    activeGraphData.selectedNode = selectedNode;
    activeGraphData.data.nodes = nodes;
    activeGraphData.data.links = links;
    setActiveGraphData((activeGraphData) => ({ ...activeGraphData }));
  };

  return (
    <div>
      <Header handleClick={handleHomeButtonClick} name={resume?.basics?.name} />
      <InfoOverlay
        windowWidth={windowState.windowWidth}
        windowHeight={windowState.windowHeight}
        selectedNode={activeGraphData.selectedNode}
      />

      <Graph
        forceGraphRef={forceGraphRef}
        data={activeGraphData.data}
        width={windowState.windowWidth}
        height={windowState.windowHeight}
        handleNodeClick={handleNodeClick}
        handleBackgroundClick={handleBackgroundClick}
      />
    </div>
  );
};

export default App;
