import { useState, useEffect } from 'react';
import config from 'lib/config';

const createGraphDataFromResume = (resume) => {
  const nodes = [];
  const links = [];

  // Construct all the skill nodes
  resume.skills.forEach((skill) => {
    const sId = `s${nodes.length}`;

    // Add this project to the node array
    nodes.push({
      ...skill,
      type: 'technology',
      id: sId,
    });

    skill.keywords &&
      skill.keywords.forEach((keyword) => {
        let tId = nodes.find((node) => node.name.toLowerCase() === keyword.toLowerCase())?.id;

        // We also need to check here that a keyword exists as a skill

        // If keyword does not exist as a node
        if (!tId) {
          tId = `t${nodes.length}`;
          nodes.push({
            name: keyword,
            type: 'technology',
            id: tId,
          });
        }

        // Create link between skill node and technology node
        links.push({
          source: sId,
          target: tId,
        });
      });
  });

  // Construct all the nodes
  resume.projects.forEach((project) => {
    const pId = `p${nodes.length}`;

    // Add this project to the node array
    nodes.push({
      ...project,
      type: 'project',
      id: pId,
    });

    project.keywords &&
      project.keywords.forEach((keyword) => {
        let tId = nodes.find((node) => node.name.toLowerCase() === keyword.toLowerCase())?.id;

        // We also need to check here that a keyword exists as a skill

        // If keyword does not exist as a node
        if (!tId) {
          tId = `t${nodes.length}`;
          nodes.push({
            name: keyword,
            type: 'technology',
            id: tId,
          });
        }

        // Create link between project node and technology node
        links.push({
          source: pId,
          target: tId,
        });
      });
  });

  return {
    nodes,
    links,
  };
};

function useResume() {
  const [resume, setResume] = useState(null);
  const [graphData, setGraphData] = useState({ nodes: [], links: [] });
  const [loading, setLoading] = useState(true);
  const [resumeError, setResumeError] = useState(null);

  useEffect(() => {
    fetch(`${config.apiUrl}/resume/${config.userId}`)
      .then((response) => {
        if (response.status !== 200) {
          throw new Error(`Bad response from server: ${response.status}`);
        }
        return response.json();
      })
      .then((response) => {
        setLoading(false);
        setResume(response);
        setGraphData(createGraphDataFromResume(response));
        setResumeError('');
      })
      .catch((error) => {
        setLoading(false);
        setResume(null);
        setGraphData({ nodes: [], links: [] });
        setResumeError(String(error));
      });
  }, []);

  return [resume, graphData, loading, resumeError];
}

export default useResume;
