module.exports = {
  apiUrl: process.env.REACT_APP_RESUME_API_URL || 'http://localhost:8000/v1',
  userId: process.env.REACT_APP_USER_ID,
  appearance: {
    technology: {
      color: 'rgba(66, 135, 245, 0.9)',
    },
    project: {
      color: 'rgba(88, 199, 54, 0.9)',
    },
    default: {
      color: 'rgba(245, 135, 66, 0.9)',
    },
  },
};
