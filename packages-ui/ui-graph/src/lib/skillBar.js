import React, { Fragment } from 'react';
import { LinearProgress } from '@mui/material';

const MIN = 0;
const MAX = 5;

// MIN = Minimum expected value
// MAX = Maximum expected value
// Function to normalise the values (MIN / MAX could be integrated)
const normalise = (value) => ((value - MIN) * 100) / (MAX - MIN);

// Example component that utilizes the `normalise` function at the point of render.
export default function SkillBar(props) {
  return (
    <Fragment>
      <LinearProgress variant="determinate" value={normalise(props.value)} />
    </Fragment>
  );
}
