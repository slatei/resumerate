import { useState, useEffect } from 'react';

const useWindow = () => {
  const [windowState, setWindowState] = useState({
    windowWidth: 0,
    windowHeight: 0,
  });

  // Update the window state on window resizing
  const updateWindowDimensions = () => {
    setWindowState((state) => ({
      ...state,
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
    }));
  };

  // Mount event listener for resizing the graph window when the browser window changes
  useEffect(() => {
    updateWindowDimensions();

    window.addEventListener('resize', updateWindowDimensions);

    return () => {
      window.removeEventListener('resize', updateWindowDimensions);
    };
  }, []);

  return [windowState, setWindowState];
};

export default useWindow;
