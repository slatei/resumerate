import React from 'react';
import { Box } from '@mui/material';

function NotFound(props) {
  return (
    <>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          marginTop: 10,
        }}
      >
        <div>{props.message || 'Something went wrong'}</div>
      </Box>
    </>
  );
}

export default NotFound;
