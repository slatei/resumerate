import React from 'react';
import { Box, Container, Link } from '@mui/material';
import SkillBar from 'lib/skillBar';
import config from 'lib/config';

const style = {
  position: 'absolute',
  zIndex: 101,
  minHeight: 1,
  borderRadius: 16,
  paddingBottom: 10,
};

const capitalize = { textTransform: 'capitalize' };

const noNodeSelected = (selectedNode) => selectedNode === null;

const isNarrowScreen = (windowWidth) => windowWidth <= 600;

const shortDate = (date) => new Date(date).toISOString().substring(0, 10);

const createDynamicStyle = (selectedNode, windowWidth, windowHeight) => {
  const dynamicStyle = {
    ...style,
    backgroundColor: config.appearance[selectedNode.type].color,
  };

  if (isNarrowScreen(windowWidth)) {
    dynamicStyle.maxWidth = windowWidth - 20;
    dynamicStyle.top = windowHeight - 200; // Puts the context window at the bottom of the screen
    dynamicStyle.left = 10;
    dynamicStyle.right = 10;
    dynamicStyle.marginBottom = 10;
  } else {
    dynamicStyle.maxWidth = windowWidth / 3;
    dynamicStyle.maxHeight = windowHeight - 40;
    dynamicStyle.top = 20;
    dynamicStyle.right = 20;
    dynamicStyle.overflowY = 'scroll';
  }
  return dynamicStyle;
};

export default function InfoOverlay(props) {
  if (noNodeSelected(props.selectedNode)) {
    return null;
  }

  const dynamicStyle = createDynamicStyle(props.selectedNode, props.windowWidth, props.windowHeight);

  return (
    <Container style={dynamicStyle}>
      <CommonProperties selectedNode={props.selectedNode} />
      <TypeProperties selectedNode={props.selectedNode} />
    </Container>
  );
}

function CommonProperties(props) {
  return (
    <div>
      <h2 style={capitalize}>{props.selectedNode.name}</h2>
      <p>{props.selectedNode.description}</p>
    </div>
  );
}

function TypeProperties(props) {
  switch (props.selectedNode.type) {
    case 'project':
      return <ProjectPropertiesDiv selectedNode={props.selectedNode} />;
    case 'technology':
      return <TechnologyPropertiesDiv selectedNode={props.selectedNode} />;
    default:
      return null;
  }
}

function ProjectPropertiesDiv(props) {
  const { selectedNode } = props;

  return (
    <>
      <Box>
        {selectedNode.startDate ? `${shortDate(selectedNode.startDate)} - ` : null}
        {selectedNode.endDate ? shortDate(selectedNode.endDate) : null}
      </Box>

      {selectedNode.url ? <Link href={selectedNode.url}>Website</Link> : null}
    </>
  );
}

function TechnologyPropertiesDiv(props) {
  const { selectedNode } = props;
  return <SkillBar value={selectedNode.level} />;
}
