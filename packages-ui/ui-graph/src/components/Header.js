import React from 'react';
import { IconButton, Box } from '@mui/material';
import { Home } from '@mui/icons-material';

const containerStyle = {
  position: 'absolute',
  zIndex: 101,
  marginTop: 10,
  marginLeft: 10,
};

const homeButtonStyle = {
  color: 'black',
};

const titleStyle = {
  marginLeft: 10,
  marginTop: 10,
};

export default function Header(props) {
  return (
    <Box display="flex" style={containerStyle}>
      <IconButton style={homeButtonStyle} onClick={props.handleClick}>
        <Home />
      </IconButton>
      <Box style={titleStyle}>{props.name || 'Personal Resume'}</Box>
    </Box>
  );
}
