import React from 'react';
import ForceGraph2D from 'react-force-graph-2d';
import config from 'lib/config';
import * as d3 from 'd3-force';

const nodeSize = (node) => node.level || 3;

function renderLabel(node, ctx, globalScale) {
  if (globalScale < 1.5) return;

  const fontSize = 8 / globalScale;
  ctx.font = `${fontSize}px "Lato"`;
  ctx.textAlign = 'center';
  ctx.textBaseline = 'middle';
  ctx.fillStyle = 'black';
  ctx.backgroundColor = 'white';
  // const bottomDistance = nodeSize(node) + 10;
  // const bottomDistance = nodeSize(node);
  ctx.fillText(node.name.toUpperCase(), node.x, node.y);
}

function Graph(props) {
  const fg = props.forceGraphRef.current;
  if (fg) {
    // Deactivate existing forces
    /* Deactivating the center force. */
    // fg.d3Force('center', null);
    // fg.d3Force('charge', null);
    fg.d3Force('charge').strength(-50);

    // Add collision and bounding box forces
    fg.d3Force('collide', d3.forceCollide(4));
    fg.d3Force('box', () => {
      const SQUARE_HALF_SIDE = props.data.nodes.length * 2;

      props.data.nodes.forEach((node) => {
        const x = node.x || 0,
          y = node.y || 0;

        // bounce on box walls
        if (Math.abs(x) > SQUARE_HALF_SIDE) {
          node.vx *= -1;
        }
        if (Math.abs(y) > SQUARE_HALF_SIDE) {
          node.vy *= -1;
        }
      });
    });
  }

  return (
    <ForceGraph2D
      ref={props.forceGraphRef}
      graphData={props.data}
      onNodeClick={props.handleNodeClick}
      onBackgroundClick={props.handleBackgroundClick}
      nodeColor={(node) => config.appearance[node.type].color}
      nodeVal={nodeSize / 10}
      nodeCanvasObject={renderLabel}
      nodeCanvasObjectMode={() => 'after'}
      width={props.width}
      height={props.height}
      // d3AlphaDecay={0} /* A d3 force that slows down the nodes. */
      // d3VelocityDecay={0}
    />
  );
}

export default Graph;
