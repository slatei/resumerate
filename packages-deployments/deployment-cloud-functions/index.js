const testApp = require('@resumapi/ms-test');
const resumeApp = require('@resumapi/ms-resume');

module.exports = { testApp, resumeApp };
