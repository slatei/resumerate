# Deployments

## Docker

Build the docker image

```bash
docker build . \
  -f packages-deployments/deployment-local/Dockerfile \
  -t resumapi:latest \
  --no-cache \
  --progress plain
```

Start the Docker service

```bash
docker run -p 8088:8088 --rm --name resumapi resumapi:latest
```
