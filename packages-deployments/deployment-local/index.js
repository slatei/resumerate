const app = require('@resumapi/ms-resume');
const { config } = require('@resumapi/lib-config');
const { startHttpServer } = require('@resumapi/lib-broker');
const { serverCert, serverKey } = require('@resumapi/lib-cert');
const logger = require('@resumapi/lib-logger');

startHttpServer({
  app,
  port: config.get('api.PORT'),
  logger,
  basePath: '/',
  sslCert: serverCert,
  sslKey: serverKey,
});
